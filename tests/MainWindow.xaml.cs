﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace tests
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static string optionsOfCube = "";
        public MainWindow()
        {
            //int numberOfCubes = 7;
            //if(!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\\options"))
            //    Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\\options");
            //string path = AppDomain.CurrentDomain.BaseDirectory + "\\options\\optionsIn" + numberOfCubes
            //    + "Cube.txt";
            //InitializeComponent();
            //if (File.Exists(path)) File.Delete(path);
            //string tmp = countCubesOptions(numberOfCubes);
            //File.AppendAllText(path, tmp);
            //Dictionary<string, int> vals = new Dictionary<string, int>();

            //foreach (string line in File.ReadAllLines(path))
            //{
            //    string num = line.Substring(line.IndexOf("=") + 1);
            //    if (vals.ContainsKey(num)) vals[num]++;
            //    else vals.Add(num, 1);
            //}

            //if (File.Exists(path + ".vals")) File.Delete(path + ".vals");
            //foreach (string num in vals.Keys)
            //{
            //    File.AppendAllText(path + ".vals", num + " has '" + vals[num] + 
            //        "' combinations\n");
            //}

            //MessageBox.Show("done");
            //Environment.Exit(0);
        }

        string countCubesOptions(int cubesCount)
        {
            string tmp = "";
            int result = 0;
            int[] nums = new int[cubesCount];
            for (int i = 0; i < cubesCount; i++)
            {
                nums[i] = 1;
            }

            while (result != 6 * cubesCount)
            {
                result = 0;
                int curLocation = 0;
                for (int i = 0; i < cubesCount; i++)
                {
                    tmp += ((i == 0) ? nums[i] + "" : "+" + nums[i]);
                    result += nums[i];
                }
                tmp += "=" + result + "\n";
                if (result == 6 * cubesCount) break;
                bool hasChanged = false;
                while(!hasChanged)
                {
                    nums[cubesCount - curLocation - 1]++;
                    if (nums[cubesCount - curLocation - 1] > 6)
                    {
                        nums[cubesCount - curLocation - 1] = 1;
                        curLocation++;
                    }
                    else hasChanged = true;

                }
            }
            

            return tmp;
        }

        int cubeNum(int cubesCount, int curNum, int lastResult, string options)
        {
            int result = 0;
            if(cubesCount == 1)
            {
                result = lastResult + curNum;
                options += ", " + curNum + " = " + result + "\n";
                if (curNum != 6)
                    cubeNum(cubesCount, curNum + 1, lastResult, options);
            }
            else
            {

            }
            optionsOfCube = options;
            return result;
        }
    }
}
