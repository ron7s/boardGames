﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace basicTests
{
    static class ComunicationHelper
    {
        public static IPAddress GetCurrentIp(bool isHamachi)
        {
            Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, 0);
            string baseAddress = (isHamachi) ? "25.0.0.0" : "192.0.0.0";
            s.Connect(baseAddress, 65530);
            IPEndPoint endPoint = s.LocalEndPoint as IPEndPoint;
            return endPoint.Address;
        }

        public static string ReciveMessage(Socket sender)
        {
            byte[] bytes = new byte[1512];
            string data;
            //List<byte> data = new List<byte>();
            try
            {
                int bytesAmount = sender.Receive(bytes);
                //data.AddRange(bytes.ToList().GetRange(0, bytesAmount));
                if (bytesAmount != 0)
                    data = Encoding.ASCII.GetString(bytes, 0, bytesAmount);
                else
                    return null;
            }
            catch
            {
                return null;
            }
            return data;
        }

        public static void SendMessage(Socket receiver, string msg)
        {
            try
            {
                receiver.Send(Encoding.ASCII.GetBytes(msg));
            }
            catch { }
        }
    }
}
