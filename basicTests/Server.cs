﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace basicTests
{
    class Server
    {
        private Socket listener;
        private List<Socket> clients;
        //private Dictionary<Socket, ClientsData> clients = new Dictionary<Socket, ClientsData>();
        
        private bool serverActive = false;

        public Server(string ip, int port)
        {
            InitServer(ip, port);
            new Thread(ConnectionListener).Start();
            while (serverActive)
            {
                string msg = Console.ReadLine();
                foreach (Socket client in clients)
                {
                    SendMessage(client, msg);
                }
            }
        }

        private void InitServer(string ipStr, int port)
        {
            clients = new List<Socket>();
            IPEndPoint ip = new IPEndPoint(IPAddress.Parse(ipStr), port);
            listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            listener.Bind(ip);
            listener.Listen(10);
            serverActive = true;
            Console.WriteLine("Server active with the ip " + ipStr + ":" + port);
        }

        private void ConnectionListener()
        {
            while (serverActive)
            {
                Socket client = listener.Accept();
                IPEndPoint clientData = client.RemoteEndPoint as IPEndPoint;
                Console.WriteLine("new connection from " + clientData.Address + ":" + clientData.Port);
                InitClients(client);
            }
        }

        private void InitClients(Socket client)
        {
            new Thread(() => ClientListener(client)).Start();
        }

        private void ClientListener(Socket client)
        {
            while (serverActive)
            {
                Thread.Sleep(20);
                string msg = ComunicationHelper.ReciveMessage(client);
                if(msg == null)
                {
                    IPEndPoint clientData = client.RemoteEndPoint as IPEndPoint;
                    Console.WriteLine("client disconnected " + clientData.Address + ":" + clientData.Port);
                    clients.Remove(client);
                    break;
                }
                else
                {
                    Console.WriteLine(msg);
                }
                
            }
        }

        private void SendMessage(Socket client, string msg)
        {
            ComunicationHelper.SendMessage(client, msg);
        }

    }
}
