﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace boardGames
{
    public class Message
    {
        public enum MessageType
        {
            Confirm, Cancel, StartGame, AnnouneWin,
            GameData, AnnouneTurn, Request, ClientData, MoveSuggestions,
            ChessMove, ChekersMove, BackgammonMove, SubmarinesHit, SubmarinesGameInfo, SubmarinesSetPositions
        }

        public enum Request
        {
            None, Restart, Undo, Redo
        }

        public Message(List<byte> data) : this(new List<byte>(), data) {}

        public Message(List<byte> prevData, List<byte> data)
        {
            List<byte> combined = new List<byte>();
            combined.AddRange(prevData);
            combined.AddRange(data);
            if (combined.Count > 5)
            {
                Size = BitConverter.ToUInt32(combined.ToArray(), 0);
                MsgType = (MessageType)Enum.Parse(typeof(MessageType), combined[4].ToString());
                combined.RemoveRange(0, 5);
                Data = combined;
            }
            else Size = 0;
        }

        public Message(MessageType type, params byte[] data)
        {
            Size = (uint)data.Length + 5;//5 = sizeof(Size) + sizeof(MessageType)
            MsgType = type;
            Data = data.ToList();
        }

        public Message(MessageType type, List<byte> data)
        {
            Size = (uint)data.Count + 5;//5 = sizeof(Size) + sizeof(MessageType)
            MsgType = type;
            Data = data;
        }

        public Message(MessageType type, string data)
        {
            Size = (uint)data.Length + 5;//5 = sizeof(Size) + sizeof(MessageType)
            MsgType = type;
            Data = Encoding.UTF8.GetBytes(data).ToList();
        }

        public uint Size { get; }

        public MessageType MsgType { get; }

        public List<byte> Data { get; }

        public byte[] ToArray()
        {
            return ToList().ToArray();
        }
        
        public List<byte> ToList()
        {
            List<byte> allData = new List<byte>();
            allData.AddRange(BitConverter.GetBytes(Size));
            allData.Add((byte)MsgType);
            allData.AddRange(Data);
            return allData;
        }

        public string GetAsText()
        {
            return Encoding.ASCII.GetString(Data.ToArray());
        }
    }
}
