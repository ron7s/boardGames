﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace boardGames.ClientRelated.gameBoards
{
    static class GameBoardsData
    {
        public static Brush MarkerBrush = Brushes.Green;
        public static Dictionary<int, Brush> playerBrush = new Dictionary<int, Brush>()
        {
            {1, Brushes.Blue },
            {2, Brushes.Red },
            {3, Brushes.Purple },
            {4, Brushes.Gold }
        };
    }
}
