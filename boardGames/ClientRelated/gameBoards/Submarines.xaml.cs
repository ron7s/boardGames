﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace boardGames.ClientRelated.gameBoards
{
    /// <summary>
    /// Interaction logic for Submarines.xaml
    /// </summary>
    public partial class Submarines : Window, GameBoardBase
    {
        internal static Queue<string> gameData = new Queue<string>();
        DispatcherTimer time = new DispatcherTimer();
        Image controledImage;
        Image targeter = new Image();
        string path = AppDomain.CurrentDomain.BaseDirectory + "\\boardGamesData\\symbols\\";
        int s2 = 1, s3 = 2, s4 = 1, s5 = 1, s6 = 0;
        int s2D = 1, s3D = 2, s4D = 1, s5D = 1, s6D = 0;
        ulong countTime = 0;
        string markedSub = "";
        bool isImageExists = false, isRotated = false, myTurn = true,
            isStartingTime = true;
        List<string> subLocations = new List<string>();

        //server vars
        bool isPlayer1 = true;
        string gameID = "";

        public Submarines()
        {
            InitializeComponent();
            fs2.Source = es2.Source = new BitmapImage(new Uri(path + "2xSub.png"));
            fs3.Source = es3.Source = new BitmapImage(new Uri(path + "3xSub.png"));
            fs4.Source = es4.Source = new BitmapImage(new Uri(path + "4xSub.png"));
            fs5.Source = es5.Source = new BitmapImage(new Uri(path + "5xSub.png"));
            fs6.Source = es6.Source = new BitmapImage(new Uri(path + "6xSub.png"));
            targeter.Source = new BitmapImage(new Uri(path + "targeter.png"));
            
            Board.Background = new ImageBrush(new BitmapImage(new Uri(path + "sea_background.jpg")));
            time.Interval = new TimeSpan(0, 0, 0, 0, 20);
            time.Tick += time_Tick;
            time.Start();
        }

        void time_Tick(object sender, EventArgs e)
        {
            //countTime += (ulong)time.Interval.Milliseconds;
            while (gameData.Count > 0)
            {
                string data = gameData.Dequeue();
                ProceessChange(data);
            }
            if (isStartingTime) createAndMoveSubs();
            else if (myTurn) setAndMoveTargeter();
            //if(countTime % 200 == 0)
            //{
                
            //}
        }

        void ProceessChange(string action)
        {
            string[] commands = action.Split('&');

            foreach (string command in commands)
            {

            }
        }

        private void fs6_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (s6 > 0 && markedSub == "" && isStartingTime) markedSub = "g6";
        }

        private void fs5_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (s5 > 0 && markedSub == "" && isStartingTime) markedSub = "g5";
        }

        private void fs4_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (s4 > 0 && markedSub == "" && isStartingTime) markedSub = "g4";
        }

        private void fs3_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (s3 > 0 && markedSub == "" && isStartingTime) markedSub = "g3";
        }

        private void fs2_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (s2 > 0 && markedSub == "" && isStartingTime) markedSub = "g2";
        }

        private void setAndMoveTargeter()
        {
            double x = Mouse.GetPosition(enemyGrid).X;
            double y = Mouse.GetPosition(enemyGrid).Y;
            if(!enemyGrid.Children.Contains(targeter)) enemyGrid.Children.Add(targeter);
            if(x >= 0 && y >= 0)
            {
                Grid.SetColumn(targeter, (int)(x / (viewGrid.Width / 10)));
                Grid.SetRow(targeter, (int)(y / (viewGrid.Height / 10)));
            }
            
            
        }

        private void createAndMoveSubs()
        {
            double x = Mouse.GetPosition(viewGrid).X;
            double y = Mouse.GetPosition(viewGrid).Y;
            if (!isImageExists && markedSub != "")
            {
                isImageExists = true;
                Image img = new Image();
                controledImage = img;
                img.Height = 20;
                img.Tag = markedSub + "&";
                img.MouseDown += img_MouseDown;
                if (markedSub == "g6")
                {
                    Grid.SetColumnSpan(img, 6);
                    img.Source = new BitmapImage(new Uri(path + "6xSub.png"));
                }
                else if (markedSub == "g5")
                {
                    Grid.SetColumnSpan(img, 5);
                    img.Source = new BitmapImage(new Uri(path + "5xSub.png"));
                }
                else if (markedSub == "g4")
                {
                    Grid.SetColumnSpan(img, 4);
                    img.Source = new BitmapImage(new Uri(path + "4xSub.png"));
                }
                else if (markedSub == "g3")
                {
                    Grid.SetColumnSpan(img, 3);
                    img.Source = new BitmapImage(new Uri(path + "3xSub.png"));
                }
                else if (markedSub == "g2")
                {
                    Grid.SetColumnSpan(img, 2);
                    img.Source = new BitmapImage(new Uri(path + "2xSub.png"));
                }
                viewGrid.Children.Add(img);
            }
            else if (markedSub != "")
            {
                try
                {
                    axis.Text = Grid.GetColumn(controledImage) + "," + Grid.GetRow(controledImage);
                    Grid.SetColumn(controledImage, (int)(x / (viewGrid.Width / 10)));
                    Grid.SetRow(controledImage, (int)(y / (viewGrid.Height / 10)));
                }
                catch{ }
                if (Grid.GetColumn(controledImage) > 10 - Grid.GetColumnSpan(controledImage))
                {
                    Grid.SetColumn(controledImage, 10 - Grid.GetColumnSpan(controledImage));
                }
                if (Grid.GetRow(controledImage) > 10 - Grid.GetRowSpan(controledImage))
                {
                    Grid.SetRow(controledImage, 10 - Grid.GetRowSpan(controledImage));
                }
            }
        }

        void img_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if(markedSub == "" && isStartingTime && Mouse.RightButton == MouseButtonState.Pressed)
            {
                controledImage = (Image)sender;
                string[] tmp = (controledImage.Tag + "").Split('&');
                isImageExists = true;
                markedSub = tmp[0];
                isRotated = (tmp[1] == "True");
                int xDir = 0, yDir = 0, x = Grid.GetColumn(controledImage), y = Grid.GetRow(controledImage);
                if (isRotated) yDir = 1;
                else xDir = 1;
                for (int i = 0; i < markedSub[1] - '0'; i++)
                {
                    subLocations.Remove((x + xDir * i) + "," + (y + yDir * i));
                }
                switch (markedSub[1])
                {
                    case '2':
                        s2++;
                        break;
                    case '3':
                        s3++;
                        break;
                    case '4':
                        s4++;
                        break;
                    case '5':
                        s5++;
                        break;
                    case '6':
                        s6++;
                        break;

                    default:
                        break;
                }
                fts2.Text = "x" + s2;
                fts3.Text = "x" + s3;
                fts4.Text = "x" + s4;
                fts5.Text = "x" + s5;
                fts6.Text = "x" + s6;
            }
        }

        void rotateShip()
        {
            isRotated = !isRotated;
            int t = Grid.GetRowSpan(controledImage);
            int t2 = Grid.GetColumnSpan(controledImage);
            Grid.SetRowSpan(controledImage, t2);
            Grid.SetColumnSpan(controledImage, t);
            double t3 = controledImage.Height;
            controledImage.Height = controledImage.Width;
            controledImage.Width = t3;
            if (isRotated) controledImage.Source = new BitmapImage(new Uri(path + markedSub[1]+ "xSubRT.png"));
            else controledImage.Source = new BitmapImage(new Uri(path + markedSub[1]+ "xSub.png"));
            
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.R && markedSub != "")
            {
                rotateShip();
            }
            if (e.Key == Key.Escape && markedSub != "")
            {
                viewGrid.Children.Remove(controledImage);
                markedSub = "";
                isRotated = false;
                isImageExists = false;
            }
        }

        private bool isLegalSubLocation(int x, int y)
        {
            int xDir = 0, yDir = 0;
            if (isRotated) yDir = 1;
            else xDir = 1;
            for (int i = 0; i < markedSub[1] - '0'; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    for (int q = -1; q <= 1; q++)
                    {
                        if (subLocations.Contains((x + xDir * i + 1 * j) + "," + (y + yDir * i + 1 * q))) return false;
                    }
                }
            }
            return true;
        }

        private void viewGrid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            int x = (int)(e.GetPosition(viewGrid).X / (viewGrid.Width / 10));
            int y = (int)(e.GetPosition(viewGrid).Y / (viewGrid.Height / 10));
            if (markedSub != "" && isLegalSubLocation(x, y) && Mouse.LeftButton == MouseButtonState.Pressed)
            {
                string tmp = markedSub;
                markedSub = "";
                if (!isRotated && x > 10 - (tmp[1] - '0')) x = 10 - (tmp[1] - '0');
                if (isRotated && y > 10 - (tmp[1] - '0')) y = 10 - (tmp[1] - '0');
                string tmp2 = controledImage.Tag + "";
                tmp2 = tmp2.Remove(tmp2.IndexOf("&"));
                tmp2 += "&" + isRotated;
                controledImage.Tag = tmp2;
                isImageExists = false;
                controledImage = null;
                int xDir = 0, yDir = 0;
                if (isRotated) yDir = 1;
                else xDir = 1;
                isRotated = false;
                for (int i = 0; i < tmp[1] - '0'; i++)
                {
                    subLocations.Add((x + xDir * i) + "," + (y + yDir * i));
                }
                switch (tmp[1])
                {
                    case '2':
                        s2--;
                        break;
                    case '3':
                        s3--;
                        break;
                    case '4':
                        s4--;
                        break;
                    case '5':
                        s5--;
                        break;
                    case '6':
                        s6--;
                        break;

                    default:
                        break;
                }
                fts2.Text = "x" + s2;
                fts3.Text = "x" + s3;
                fts4.Text = "x" + s4;
                fts5.Text = "x" + s5;
                fts6.Text = "x" + s6;
            }
            else if (markedSub != "" && Mouse.LeftButton == MouseButtonState.Pressed) MessageBox.Show("מיקום הצוללת לא חוקי");

        }

        private void clearBoard_Click(object sender, RoutedEventArgs e)
        {
            s2 = s2D; s3 = s3D; s4 = s4D; s5 = s5D; s6 = s6D;
            viewGrid.Children.Clear();
            subLocations.Clear();
            markedSub = "";
            isRotated = false;
            isImageExists = false;
            fts2.Text = "x" + s2;
            fts3.Text = "x" + s3;
            fts4.Text = "x" + s4;
            fts5.Text = "x" + s5;
            fts6.Text = "x" + s6;
        }

        private void ready_Click(object sender, RoutedEventArgs e)
        {
            if(s2 + s3 + s4 + s5 + s6 == 0)
            {
                isStartingTime = false;
                MainWindow.SendMessage("<setData>" + gameID + "&" + ((isPlayer1) ? "player1&" : "player2&")
                    + s2D + "&" + s3D + "&" + s4D + "&" + s5D + "&" + s6D);
                ready.IsEnabled = clearBoard.IsEnabled = false;
            }
            //string dataAsSingleString = "";
            //for (int i = 0; i < subLocations.Count; i++)
            //{
            //    dataAsSingleString += subLocations[i] + "" + "&";
            //}
            //dataAsSingleString = dataAsSingleString.Remove(dataAsSingleString.Length - 1, 1);
            //MainWindow.SendMessage("<submarinesData>" + gameID + "&" + ((isPlayer1) ? "player1&" : "player2&")
            //        + dataAsSingleString);

            
        }

        private void enemyGrid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if(!isStartingTime && myTurn)
            {
                myTurn = false;
                //MainWindow.SendMessage("<submarinesMove>" + gameID + "&" + ((isPlayer1)? "player1&":"player2&")
                //    + Grid.GetColumn(targeter) + "," + Grid.GetRow(targeter));
                enemyGrid.Children.Remove(targeter);
            }
        }

        public void RestartGame()
        {
            throw new NotImplementedException();
        }

        public void EnqueueGamePieces(byte[] piecesData)
        {
            throw new NotImplementedException();
        }

        public void EnqueueSuggestions(byte[] piecesData)
        {
            throw new NotImplementedException();
        }
    }
}
