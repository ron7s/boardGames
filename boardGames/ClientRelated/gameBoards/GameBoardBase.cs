﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace boardGames.ClientRelated.gameBoards
{
    public interface GameBoardBase
    {
        void EnqueueGamePieces(byte[] piecesData);
        void EnqueueSuggestions(byte[] piecesData);
        void Close();
    }
}
