﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;
using System.Windows.Media;

namespace boardGames.ClientRelated.gameBoards
{
    class checkersPlayer : Player
    {
        Ellipse el = new Ellipse();
        int playerNumber = -1, x = -1, y = -1;
        bool isWhite = true, isKing = false, hasCrownDrown = false;

        public checkersPlayer(int playerNumber, Ellipse el, int x, int y)
        {
            this.playerNumber = playerNumber;
            this.el = el;
            this.x = x;
            this.y = y;
            if (el.Fill == Brushes.Red) isWhite = true;
            else isWhite = false;
        }

        public void SetToDefaultColor()
        {
            if (isWhite) el.Fill = Brushes.Red;
            else el.Fill = Brushes.Blue;
        }

        public Ellipse ActualEllipse
        {
            get
            {
                return el;
            }
        }

        public bool IsKing
        {
            get
            {
                return isKing;
            }
            set
            {
                isKing = value;
            }
        }
        public bool IsWhite
        {
            get
            {
                return isWhite;
            }
        }
        public int PlayerNumber
        {
            get
            {
                return playerNumber;
            }
        }
        public int X
        {
            get
            {
                return x;
            }
        }
        public int Y
        {
            get
            {
                return y;
            }
        }
        public bool HasCrownDrown
        {
            get
            {
                return hasCrownDrown;
            }
            set
            {
                hasCrownDrown = value;
            }
        }
    }
}
