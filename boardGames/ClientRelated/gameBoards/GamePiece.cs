﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace boardGames.ClientRelated.gameBoards
{
    class GamePiece : Canvas
    {
        Brush color;

        public GamePiece(int player)
        {
            Player = player;
        }

        public bool IsPlayer(int player)
        {
            return Player == player;
        }

        public int Player { get; }

        public GamePieceType Type { get; set; }

        public bool Alive { get; set; }

        public int Position { get; set; }

        public bool Marked
        {
            set
            {
                if (value)
                {
                    Fill(GameBoardsData.MarkerBrush);
                }
                else
                {
                    Fill(GameBoardsData.playerBrush.ElementAt(Player).Value);
                }
            }
        }

        public void Fill(Brush brush)
        {
            color = brush;
            foreach (UIElement item in Children)
            {
                if(item is Shape shape)
                {
                    shape.Fill = brush;
                }
            }
        }

        public void DrawPiece(Canvas board, int x, int y)
        {
            switch (Type)
            {
                case GamePieceType.CheckersPiece:
                    DrawChekersPiece(board, x, y);
                    break;
                case GamePieceType.CheckersKing:
                    DrawChekersPiece(board, x, y);
                    DrawChekersCrown();
                    break;
                case GamePieceType.ChessPawn:
                    break;
                case GamePieceType.ChessRunner:
                    break;
                case GamePieceType.ChessRook:
                    break;
                case GamePieceType.ChessKnight:
                    break;
                case GamePieceType.ChessQueen:
                    break;
                case GamePieceType.ChessKing:
                    break;
                case GamePieceType.BacgammonPiece:
                    break;
                case GamePieceType.Submarine:
                    break;
                default:
                    break;
            }
            lock (board.Children)
            {
                board.Children.Add(this);
            }
        }

        private void DrawChekersPiece(Canvas board, int x, int y)
        {
            double widthCenterer = (board.Width - 60 * 8) / 2 + 5;
            double heightCenterer = (board.Height - 60 * 8) / 2 + 5;
            Ellipse piece = new Ellipse
            {
                Fill = color,
                Width = 50,
                Height = 50
            };
            SetLeft(this, x * 60 + widthCenterer);
            SetTop(this, y * 60 + heightCenterer);
            Children.Add(piece);
        }

        private void DrawChekersCrown()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "\\boardGamesData\\symbols\\crown.png";
            Image crown = new Image
            {
                Source = new BitmapImage(new Uri(path)),
                Width = 32,
                Height = 28
            };
            SetLeft(crown, 9);
            SetTop(crown, 11);
            Children.Add(crown);
        }

    }
}
