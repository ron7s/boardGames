﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace boardGames.ClientRelated.gameBoards
{
    /// <summary>
    /// Interaction logic for Checkers.xaml
    /// </summary>
    public partial class Checkers : Window, GameBoardBase
    {
        static Dictionary<int, string> oldBlackPl, oldWhitePl;
        static List<int> oldKings;
        static Dictionary<int, Player> players = new Dictionary<int, Player>();
        public static Brush blackBrush = Brushes.Blue;
        public static Brush whiteBrush = Brushes.Red;
        static Brush markBrush = Brushes.Green;
        //static bool hasGameStarted = false, isPrivateGame = false;
        static bool isWhitePlayer = true, canEffectGame = true, canCominceTurn = false;
        static string gameId = "";//, gameName = "", userName = "";
        internal static Queue<string> gameData = new Queue<string>();
        DispatcherTimer time = new DispatcherTimer();


        public Checkers()
        {
            InitializeComponent();
            board.Width = 900;
            board.Height = 610;
            time.Interval = new TimeSpan(0, 0, 0, 0, 20);
            time.Tick += time_Tick;
            time.Start();
            gameData = new Queue<string>();
            Canvas.SetTop(this, (SystemParameters.PrimaryScreenHeight - this.Height) / 2);
            Canvas.SetLeft(this, (SystemParameters.PrimaryScreenWidth - this.Width) / 2);
            drawBoard();
        }
        public void EndGame(string winner)
        {
            clearSymbolsFromBoard();
            clearColorsFromBoard();
            List<UIElement> ITR = new List<UIElement>();
            foreach (UIElement item in board.Children)
            {
                if (item is Ellipse)
                {
                    ITR.Add(item);
                }
            }
            foreach (UIElement item in ITR)
            {
                board.Children.Remove(item);
            }
            ITR.Clear();
            players.Clear();
            markBrush = (winner == "white") ? whiteBrush : blackBrush;
            WinAnnounce wa = new WinAnnounce(markBrush);
            wa.Show();
            MainWindow.Games.Remove(gameId);
            Close();
        }

        void time_Tick(object sender, EventArgs e)
        {
            while (gameData.Count > 0)
            {
                string data = gameData.Dequeue();
                ProceessChange(data);
            }

        }

        #region drawings
        private void drawBoard()
        {
            Rectangle[,] cells = new Rectangle[8, 8];
            double widthCenterer = (board.Width - 60 * 8) / 2;
            double heightCenterer = (board.Height - 60 * 8) / 2;
            for (int x = 0; x < 8; x++)
            {
                for (int y = 0; y < 8; y++)
                {
                    cells[x, y] = new Rectangle();
                    Rectangle r = cells[x, y];
                    Canvas.SetLeft(r, x * 60 + widthCenterer);
                    Canvas.SetTop(r, y * 60 + heightCenterer);
                    r.Width = 60;
                    r.Height = 60;

                    if (x % 2 == 0 && y % 2 == 0) r.Fill = Brushes.White;
                    else if (x % 2 == 0)
                    {
                        r.Fill = Brushes.Black;
                        r.MouseLeftButtonDown += r_MouseLeftButtonDown;
                    }
                    else if (y % 2 != 0) r.Fill = Brushes.White;
                    else
                    {
                        r.Fill = Brushes.Black;
                        r.MouseLeftButtonDown += r_MouseLeftButtonDown;
                    }
                    board.Children.Add(r);
                }
            }
        }

        void drawPlayers(Dictionary<int, string> blackPlayers, Dictionary<int, string> whitePlayers, List<int> kings)
        {
            clearSymbolsFromBoard();
            clearColorsFromBoard();
            List<UIElement> ITR = new List<UIElement>();
            foreach (UIElement item in board.Children)
            {
                if (item is Ellipse)
                {
                    ITR.Add(item);
                }
            }
            foreach (UIElement item in ITR)
            {
                board.Children.Remove(item);
            }
            ITR.Clear();
            players.Clear();

            double widthCenterer = (board.Width - 60 * 8) / 2 + 5;
            double heightCenterer = (board.Height - 60 * 8) / 2 + 5;
            foreach (int pNum in blackPlayers.Keys)
            {
                string player = blackPlayers[pNum];
                if (player != "eaten")
                {
                    int x = int.Parse(player.Remove(player.IndexOf(",")));
                    int y = int.Parse(player.Substring(player.IndexOf(",") + 1));
                    Ellipse pl = new Ellipse();
                    pl.MouseLeftButtonDown += pl_MouseLeftButtonDown;
                    pl.Fill = blackBrush;
                    pl.Height = 50;
                    pl.Width = 50;
                    Canvas.SetLeft(pl, x * 60 + widthCenterer);
                    Canvas.SetTop(pl, y * 60 + heightCenterer);
                    players.Add(pNum, new checkersPlayer(pNum, pl, x, y));
                    Image king = new Image();
                    if (kings.Contains(pNum))
                    {
                        string path = AppDomain.CurrentDomain.BaseDirectory + "\\boardGamesData\\symbols\\crown.png";
                        ((checkersPlayer)players[pNum]).IsKing = true;
                        king.Source = new BitmapImage(new Uri(path));
                        king.Name = "n" + (pNum);
                        king.Width = 32;
                        king.Height = 28;
                        king.MouseLeftButtonDown += king_MouseLeftButtonDown;
                        Canvas.SetLeft(king, x * 60 + widthCenterer + 9);
                        Canvas.SetTop(king, y * 60 + heightCenterer + 11);
                    }
                    board.Children.Add(pl);
                    board.Children.Add(king);
                }
            }
            foreach (int pNum in whitePlayers.Keys)
            {
                string player = whitePlayers[pNum];
                if (player != "eaten")
                {
                    int x = int.Parse(player.Remove(player.IndexOf(",")));
                    int y = int.Parse(player.Substring(player.IndexOf(",") + 1));
                    Ellipse pl = new Ellipse();
                    pl.MouseLeftButtonDown += pl_MouseLeftButtonDown;
                    pl.Fill = whiteBrush;
                    pl.Height = 50;
                    pl.Width = 50;
                    Canvas.SetLeft(pl, x * 60 + widthCenterer);
                    Canvas.SetTop(pl, y * 60 + heightCenterer);
                    players.Add(pNum + 12, new checkersPlayer(pNum + 12, pl, x, y));
                    Image king = new Image();
                    if (kings.Contains(pNum + 12))
                    {
                        string path = AppDomain.CurrentDomain.BaseDirectory + "\\boardGamesData\\symbols\\crown.png";
                        ((checkersPlayer)players[pNum + 12]).IsKing = true;
                        king.Source = new BitmapImage(new Uri(path));
                        king.Name = "n" + (pNum + 12);
                        king.Width = 32;
                        king.Height = 28;
                        king.MouseLeftButtonDown += king_MouseLeftButtonDown;
                        Canvas.SetLeft(king, x * 60 + widthCenterer + 9);
                        Canvas.SetTop(king, y * 60 + heightCenterer + 11);
                    }
                    board.Children.Add(pl);
                    board.Children.Add(king);
                }
            }
        }

        internal void saveLastState(Dictionary<int, string> blackPlayers,
                Dictionary<int, string> whitePlayers, List<int> kings)
        {
            oldBlackPl = blackPlayers;
            oldWhitePl = whitePlayers;
            oldKings = kings;
        }

        void clearColorsFromBoard()
        {
            foreach (UIElement item in board.Children)
            {
                if (item is Ellipse)
                {
                    if (((Ellipse)item).Fill == markBrush)
                    {
                        ((Ellipse)item).Fill = ((isWhitePlayer) ? whiteBrush : blackBrush);
                    }
                }
                else if (item is Rectangle)
                {
                    if (((Rectangle)item).Fill == markBrush)
                    {
                        ((Rectangle)item).Fill = Brushes.Black;
                    }
                }
            }
        }
        #endregion

        void ProceessChange(string action)
        {
            string[] players = action.Split('&');
            Dictionary<int, string> blackPlayers = new Dictionary<int, string>();
            Dictionary<int, string> whitePlayers = new Dictionary<int, string>();
            List<int> kings = new List<int>();
            foreach (string player in players)
            {
                string pData = player.Remove(0, 6);
                if (player.IndexOf("SData:") == 0)
                {
                    if (pData == "spectator") canEffectGame = false;
                    else
                    {
                        canEffectGame = true;
                        if (pData == "black") isWhitePlayer = false;
                        else
                        {
                            isWhitePlayer = true;
                            canCominceTurn = true;
                        }
                    }
                }
                else if (player.IndexOf("romId:") == 0)
                {
                    gameId = pData;
                    if (!MainWindow.Games.ContainsKey(gameId)) MainWindow.Games.Add(gameId, this);
                }
                else if (player.IndexOf("CTurn:") == 0)
                {
                    if (pData == "white")
                    {
                        if (isWhitePlayer) canCominceTurn = true;
                        else canCominceTurn = false;
                    }
                    else
                    {
                        if (isWhitePlayer) canCominceTurn = false;
                        else canCominceTurn = true;
                    }
                }
                else if (player.IndexOf("white:") == 0)
                {
                    bool isKing = (pData.Remove(pData.IndexOf(":")) == "k");
                    pData = pData.Remove(0, 2);
                    whitePlayers.Add((int.Parse(pData.Remove
                        (pData.IndexOf("=")))), pData.Substring(pData.IndexOf("=") + 1));
                    if (isKing) kings.Add((int.Parse(pData.Remove(pData.IndexOf("=")))) + 12);
                }
                else if (player.IndexOf("black:") == 0)
                {
                    bool isKing = (pData.Remove(pData.IndexOf(":")) == "k");
                    pData = pData.Remove(0, 2);
                    blackPlayers.Add((int.Parse(pData.Remove(pData.IndexOf("=")))),
                        pData.Substring(pData.IndexOf("=") + 1));
                    if (isKing) kings.Add((int.Parse(pData.Remove(pData.IndexOf("=")))));
                }
            }
            saveLastState(blackPlayers, whitePlayers, kings);
            drawPlayers(blackPlayers, whitePlayers, kings);
        }

        void clearSymbolsFromBoard()
        {
            List<UIElement> ITR = new List<UIElement>();
            foreach (UIElement item in board.Children)
            {
                if (item is Image)
                {
                    ITR.Add(item);
                }
            }
            foreach (UIElement item in ITR)
            {
                board.Children.Remove(item);
            }
        }

        Dictionary<int, List<int>> sugestedLocations(int x, int y, bool isWhite, bool isKing)
        {
            Dictionary<int, List<int>> locations = new Dictionary<int, List<int>>();
            if (!isKing)
            {
                bool canGoToRight = true, canGoToLeft = true;
                bool canEatRight = true, canEatLeft = true;
                if (isWhite)
                {
                    foreach (int i in players.Keys)
                    {
                        checkersPlayer pl = (checkersPlayer)players[i];
                        if (pl.X == x + 1 && pl.Y == y - 1)
                        {
                            canGoToRight = false;
                            if (pl.IsWhite) canEatRight = false;
                            else
                            {
                                foreach (checkersPlayer player in players.Values)
                                {
                                    if ((player.X == x + 2 && player.Y == y - 2) || (x + 2) > 7 || (y - 2) < 0)
                                    {
                                        canEatRight = false;
                                        break;
                                    }
                                }
                            }
                        }
                        else if (pl.X == x - 1 && pl.Y == y - 1)
                        {
                            canGoToLeft = false;
                            if (pl.IsWhite) canEatLeft = false;
                            else
                            {
                                foreach (checkersPlayer player in players.Values)
                                {
                                    if ((player.X == x - 2 && player.Y == y - 2) || (x - 2) < 0 || (y - 2) < 0)
                                    {
                                        canEatLeft = false;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    List<int> tmp = new List<int>();
                    if (canGoToLeft) tmp.Add(x - 1);
                    if (canGoToRight) tmp.Add(x + 1);
                    if (tmp.Count > 0) locations.Add(y - 1, tmp);
                    List<int> tmp2 = new List<int>();
                    if (!canGoToLeft && canEatLeft) tmp2.Add(x - 2);
                    if (!canGoToRight && canEatRight) tmp2.Add(x + 2);
                    if (tmp2.Count > 0) locations.Add(y - 2, tmp2);
                }
                else
                {
                    foreach (int i in players.Keys)
                    {
                        checkersPlayer pl = (checkersPlayer)players[i];
                        if (pl.X == x + 1 && pl.Y == y + 1)
                        {
                            canGoToRight = false;
                            if (!pl.IsWhite) canEatRight = false;
                            else
                            {
                                foreach (checkersPlayer player in players.Values)
                                {
                                    if ((player.X == x + 2 && player.Y == y + 2) || (x + 2) > 7 || (y + 2) > 7)
                                    {
                                        canEatRight = false;
                                        break;
                                    }
                                }
                            }
                        }
                        else if (pl.X == x - 1 && pl.Y == y + 1)
                        {
                            canGoToLeft = false;
                            if (!pl.IsWhite) canEatLeft = false;
                            else
                            {
                                foreach (checkersPlayer player in players.Values)
                                {
                                    if ((player.X == x - 2 && player.Y == y + 2) || (x - 2) < 0 || (y + 2) > 7)
                                    {
                                        canEatLeft = false;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    List<int> tmp = new List<int>();
                    if (canGoToRight) tmp.Add(x + 1);
                    if (canGoToLeft) tmp.Add(x - 1);
                    if (tmp.Count > 0) locations.Add(y + 1, tmp);
                    List<int> tmp2 = new List<int>();
                    if (!canGoToRight && canEatRight) tmp2.Add(x + 2);
                    if (!canGoToLeft && canEatLeft) tmp2.Add(x - 2);
                    if (tmp2.Count > 0) locations.Add(y + 2, tmp2);
                }

            }
            else
            {
                
                List<int>[] Ypoints = new List<int>[8];
                for (int i = 0; i < 8; i++)
                {
                    Ypoints[i] = new List<int>();
                }

                bool canContinueM = true, canContinueP = true;
                for (int ay = y + 1; ay < 8; ay++)
                {
                    int diffrence = ay - y;
                    foreach (checkersPlayer pl in players.Values)
                    {
                        if(pl.Y == ay)
                        {
                            if (pl.X == x + diffrence && canContinueP)
                            {
                                canContinueP = false;
                                if(isWhite != pl.IsWhite)
                                {
                                    bool canMove = true;
                                    foreach (checkersPlayer pl2 in players.Values)
                                    {
                                        if(pl2.X == pl.X + 1 && pl2.Y == pl.Y + 1)
                                        {
                                            canMove = false;
                                            break;
                                        }
                                    }
                                    if (canMove && ay < 7)
                                    {
                                        Ypoints[ay + 1].Add(x + diffrence + 1);
                                    }
                                }
                            }
                            else if(pl.X == x - diffrence && canContinueM)
                            {
                                canContinueM = false;
                                if (isWhite != pl.IsWhite)
                                {
                                    bool canMove = true;
                                    foreach (checkersPlayer pl2 in players.Values)
                                    {
                                        if (pl2.X == pl.X - 1 && pl2.Y == pl.Y + 1)
                                        {
                                            canMove = false;
                                            break;
                                        }
                                    }
                                    if (canMove && ay < 7)
                                    {
                                        Ypoints[ay + 1].Add(x - diffrence - 1);
                                    }
                                }
                            }
                        }
                    }
                    if (canContinueP)
                    {
                        Ypoints[ay].Add(x + diffrence);
                    }
                    if (canContinueM)
                    {
                        Ypoints[ay].Add(x - diffrence);
                    }
                }
                canContinueM = true; canContinueP = true;
                for (int my = y - 1; my >= 0; my--)
                {
                    int diffrence = y - my;
                    foreach (checkersPlayer pl in players.Values)
                    {
                        if (pl.Y == my)
                        {
                            if (pl.X == x + diffrence && canContinueP)
                            {
                                canContinueP = false;
                                if (isWhite != pl.IsWhite)
                                {
                                    bool canMove = true;
                                    foreach (checkersPlayer pl2 in players.Values)
                                    {
                                        if (pl2.X == pl.X + 1 && pl2.Y == pl.Y - 1)
                                        {
                                            canMove = false;
                                            break;
                                        }
                                    }
                                    if (canMove && my > 0)
                                    {
                                        Ypoints[my - 1].Add(x + diffrence + 1);
                                    }
                                }
                            }
                            else if (pl.X == x - diffrence && canContinueM)
                            {
                                canContinueM = false;
                                if (isWhite != pl.IsWhite)
                                {
                                    bool canMove = true;
                                    foreach (checkersPlayer pl2 in players.Values)
                                    {
                                        if (pl2.X == pl.X - 1 && pl2.Y == pl.Y - 1)
                                        {
                                            canMove = false;
                                            break;
                                        }
                                    }
                                    if (canMove && my > 0)
                                    {
                                        Ypoints[my - 1].Add(x - diffrence - 1);
                                    }
                                }
                            }
                        }
                    }
                    if (canContinueP)
                    {
                        Ypoints[my].Add(x + diffrence);
                    }
                    if (canContinueM)
                    {
                        Ypoints[my].Add(x - diffrence);
                    }
                }

                for (int i = 0; i < Ypoints.Length; i++)
                {
                    if (Ypoints[i].Count > 0) locations.Add(i, Ypoints[i]);
                }
            }
            
            return locations;
        }

        void king_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Image tmp = (Image)sender;
            int num = int.Parse(tmp.Name.Remove(0, 1));
            pl_MouseLeftButtonDown(players[num], e);
        }

        void pl_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)//player click
        {
            double widthCenterer = (board.Width - 60 * 8) / 2;
            double heightCenterer = (board.Height - 60 * 8) / 2;
            clearColorsFromBoard();
            Ellipse el;
            if(sender is Ellipse)
            {
                el = (Ellipse)sender;
            }
            else
            {
                el = ((checkersPlayer)sender).ActualEllipse;
            }
            if (el.Fill == ((isWhitePlayer) ? whiteBrush : blackBrush) && canEffectGame && canCominceTurn)
            {
                el.Fill = markBrush;
                foreach (int i in players.Keys)
                {
                    if (players[i] is checkersPlayer)
                    {
                        checkersPlayer pl = (checkersPlayer)players[i];
                        if (pl.ActualEllipse == el)
                        {
                            Dictionary<int, List<int>> locations = sugestedLocations(pl.X, pl.Y, pl.IsWhite, pl.IsKing);
                            foreach (int cur in locations.Keys)
                            {
                                foreach (UIElement item in board.Children)
                                {
                                    if (item is Rectangle)
                                    {
                                        for (int j = 0; j < locations[cur].Count; j++)
                                        {
                                            if (cur * 60 + heightCenterer == Canvas.GetTop(item) &&
                                                locations[cur][j] * 60 + widthCenterer == Canvas.GetLeft(item))
                                            {
                                                ((Rectangle)item).Fill = markBrush;
                                            }
                                        }
                                    }
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }

        void r_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)//board click
        {
            double widthCenterer = (board.Width - 60 * 8) / 2;
            double heightCenterer = (board.Height - 60 * 8) / 2;

            Rectangle r = (Rectangle)sender;
            if (r.Fill == markBrush)
            {
                foreach (checkersPlayer player in players.Values)
                {
                    if (player.ActualEllipse.Fill == markBrush)
                    {
                        //client side set to location useful for slow internet
                        /*
                        Canvas.SetTop(player.ActualEllipse, Canvas.GetTop(r) + 5);
                        Canvas.SetLeft(player.ActualEllipse, Canvas.GetLeft(r) + 5);
                        clearColorsFromBoard();
                         */

                        int serverNumber = (!isWhitePlayer) ? player.PlayerNumber : player.PlayerNumber - 12;
                        double x = ((Canvas.GetLeft(r) - widthCenterer) / 60);
                        double y = ((Canvas.GetTop(r) - heightCenterer) / 60);
                        //ID&color&number&locationX&locationY
                        MainWindow.SendMessage("<moveCheckers>" + gameId + "&" + ((isWhitePlayer) ? "white&" : "black&") + serverNumber
                            + "&" + x + "&" + y);
                        canCominceTurn = false;
                    }
                }
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            MainWindow.hasSessionEnded = true;
        }

        public void RestartGame()
        {
            throw new NotImplementedException();
        }

        public void EnqueueGamePieces(byte[] piecesData)
        {
            throw new NotImplementedException();
        }

        public void EnqueueSuggestions(byte[] piecesData)
        {
            throw new NotImplementedException();
        }
    }
}
