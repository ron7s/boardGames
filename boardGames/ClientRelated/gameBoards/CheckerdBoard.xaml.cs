﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace boardGames.ClientRelated.gameBoards
{
    /// <summary>
    /// Interaction logic for CheckerdBoard.xaml
    /// </summary>
    public partial class CheckerdBoard : Window, GameBoardBase
    {
        static readonly DispatcherTimer time = new DispatcherTimer();
        static Queue<byte[]> piecesRefresher;
        static Queue<byte[]> suggestionsRefresher;
        readonly RoomType gameType;
        bool canClose = false;


        public CheckerdBoard(RoomType gameType)
        {
            InitializeComponent();
            piecesRefresher = new Queue<byte[]>();
            suggestionsRefresher = new Queue<byte[]>();
            this.gameType = gameType;
            board.Width = 900;
            board.Height = 610;
            time.Interval = new TimeSpan(0, 0, 0, 0, 50);
            time.Tick += Time_Tick;
            time.Start();
            Canvas.SetTop(this, (SystemParameters.PrimaryScreenHeight - this.Height) / 2);
            Canvas.SetLeft(this, (SystemParameters.PrimaryScreenWidth - this.Width) / 2);
            StartNewGame();
        }

        public void StartNewGame()
        {
            DrawBoard();
        }

        public void EnqueueGamePieces(byte[] piecesData)
        {
            lock(piecesRefresher)
            {
                piecesRefresher.Enqueue(piecesData);
            }
        }

        public void EnqueueSuggestions(byte[] suggestionsData)
        {
            lock (suggestionsRefresher)
            {
                suggestionsRefresher.Enqueue(suggestionsData);
            }
        }

        public new void Close()
        {
            canClose = true;
        }

        #region events
        private void Time_Tick(object sender, EventArgs e)
        {
            byte[] piecesData = null;
            byte[] suggestionsData = null;
            lock (piecesRefresher)
            {
                if (piecesRefresher.Count > 0)
                {
                    piecesData = piecesRefresher.Dequeue();
                }
            }
            if(piecesData != null)
            {
                DrawGamePieces(piecesData);
            }

            lock (suggestionsRefresher)
            {
                if (suggestionsRefresher.Count > 0)
                {
                    suggestionsData = suggestionsRefresher.Dequeue();
                }
            }
            if (suggestionsData != null)
            {
                DrawSuggestions(suggestionsData);
            }

            if(canClose)
            {
                base.Close();
            }
        }

        private void Piece_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            GamePiece piece = (GamePiece)sender;
            object controledPlayer = Client.GetData("control");
            if (Convert.ToInt32(controledPlayer) != -1 && piece.Player == Convert.ToInt32(controledPlayer))
            {
                Client.SetData("pos", piece.Position);
                Client.SendMessage(Message.MessageType.MoveSuggestions, (byte)piece.Player, (byte)piece.Position);
            }
        }

        private void Rect_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {//playerNum, position, x, y
            Rectangle rect = (Rectangle)sender;
            int player = Convert.ToInt32(Client.GetData("control"));
            Client.SendMessage(Message.MessageType.ChekersMove, (byte)player, Convert.ToByte(Client.GetData("pos")),
                (byte)((int[])rect.Tag)[0], (byte)((int[])rect.Tag)[1]);
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            MainWindow.hasSessionEnded = true;
        }
        #endregion

        #region drawings
        private void DrawBoard()
        {
            Rectangle[,] cells = new Rectangle[8, 8];
            double widthCenterer = (board.Width - 60 * 8) / 2;
            double heightCenterer = (board.Height - 60 * 8) / 2;
            for (int x = 0; x < 8; x++)
            {
                for (int y = 0; y < 8; y++)
                {
                    cells[x, y] = new Rectangle();
                    Rectangle r = cells[x, y];
                    Canvas.SetLeft(r, x * 60 + widthCenterer);
                    Canvas.SetTop(r, y * 60 + heightCenterer);
                    r.Width = 60;
                    r.Height = 60;

                    if (x % 2 == 0 && y % 2 == 0)
                    {
                        r.Fill = Brushes.White;
                        r.Tag = new int[] { x, y, 0, 1 };
                    }
                    else if (x % 2 == 0)
                    {
                        r.Fill = Brushes.Black;
                        r.Tag = new int[] { x, y, 0, 0 };
                    }
                    else if (y % 2 != 0)
                    {
                        r.Fill = Brushes.White;
                        r.Tag = new int[] { x, y, 0, 1 };
                    }
                    else
                    {
                        r.Fill = Brushes.Black;
                        r.Tag = new int[] { x, y, 0, 0 };
                    }
                    lock (board.Children)
                    {
                        board.Children.Add(r);
                    }
                }
            }
        }

        public void DrawGamePieces(byte[] piecesData)//playerNum, position, isKing, X, Y
        {
            ClearMarkedElements();
            ClearGamePiecesFromBoard();

            switch (gameType)
            {
                case RoomType.Chekers:
                    DrawChekersPieces(piecesData);
                    break;
                case RoomType.Chess:
                    DrawChessPieces(piecesData);
                    break;
                default:
                    break;
            }
        }

        public void DrawSuggestions(byte[] suggestionsData)
        {
            ClearMarkedElements();
            lock (board.Children)
            {
                foreach (UIElement item in board.Children)
                {
                    if(item is GamePiece piece && piece.Player == suggestionsData[0] && piece.Position == suggestionsData[1])
                    {
                        piece.Marked = true;
                        break;
                    }
                }
                for (int i = 2; i < suggestionsData.Length; i += 2)
                {
                    foreach (UIElement item in board.Children)
                    {
                        if (item is Rectangle)
                        {
                            Rectangle rect = item as Rectangle;
                            int[] pos = (int[])rect.Tag;
                            if (pos[0] == suggestionsData[i] && pos[1] == suggestionsData[i + 1])
                            {
                                ((int[])rect.Tag)[2] = 1;
                                rect.MouseLeftButtonDown += Rect_MouseLeftButtonDown;
                                rect.Fill = GameBoardsData.MarkerBrush;
                            }
                        }
                    }
                }
            }
        }

        void DrawChekersPieces(byte[] piecesData)
        {//playerNum, position, isKing, X, Y
            for (int i = 0; i < piecesData.Length; i+=5)
            {
                GamePiece piece = new GamePiece(piecesData[i]);
                piece.Position = piecesData[i + 1];
                piece.Type = (piecesData[i + 2] > 0)? GamePieceType.CheckersKing : GamePieceType.CheckersPiece;
                piece.Marked = false;
                piece.DrawPiece(board, piecesData[i + 3], piecesData[i + 4]);
                piece.MouseLeftButtonDown += Piece_MouseLeftButtonDown;
                
            }
        }

        void DrawChessPieces(byte[] piecesData)
        {
            
        }

        /// <summary>
        /// remove markers and events from board and from gamePieces
        /// </summary>
        void ClearMarkedElements()
        {
            lock (board.Children)
            {
                foreach (UIElement item in board.Children)
                {
                    if (item is GamePiece)
                    {
                        (item as GamePiece).Marked = false;
                    }
                    else if (item is Rectangle rect)
                    {//I setted on creation the object Rectangle.Tag to int[4]: [0]=X, [1]=Y, [2]=isMarked, [3]=isWhite
                        int[] rectData = (int[])rect.Tag;
                        if (rectData[2] == 1)
                        {
                            rect.Fill = rectData[3] == 1 ? Brushes.White : Brushes.Black;
                            rect.MouseLeftButtonDown -= Rect_MouseLeftButtonDown;
                            rectData[2] = 0;
                        }
                    }
                }
            }
        }

        void ClearGamePiecesFromBoard()
        {
            lock (board.Children)
            {
                List<UIElement> toRemove = new List<UIElement>();
                foreach (UIElement item in board.Children)
                {
                    if (item is GamePiece)
                    {
                        toRemove.Add(item);
                    }
                }
                foreach (UIElement item in toRemove)
                {
                    board.Children.Remove(item);
                }
            }
        }
        #endregion
    }
}
