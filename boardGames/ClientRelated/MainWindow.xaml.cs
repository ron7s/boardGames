﻿using System;
using System.IO;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO.Compression;

using System.Net;
using System.Net.Sockets;
using System.Windows.Threading;
using boardGames.ClientRelated.gameBoards;
using boardGames.ServerRelated;

namespace boardGames.ClientRelated
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static Socket sender;
        IPEndPoint ipAdress;
        readonly Queue<string> dataQ = new Queue<string>();
        readonly DispatcherTimer infpr = new DispatcherTimer();
        bool hasGameStarted = false, hasConnected = false, isPrivateGame = false;
        internal static bool hasSessionEnded = true;
        string missedData = "", gameId = "", gameName = "", userName = "";
        public static Dictionary<string, Window> Games = new Dictionary<string, Window>();
        readonly Dictionary<string, Action> GameInitDictionary = new Dictionary<string, Action>();

        public MainWindow()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory;
            while(Directory.Exists(path + "ronsTmpUpdateDirWillBeDeletedShortly"))
            {
                Directory.Delete(path + "ronsTmpUpdateDirWillBeDeletedShortly", true);
            }
            path += "boardGamesData";
            if (!Directory.Exists(path + "\\symbols"))
            {
                Directory.CreateDirectory(path);
                WebClient wc = new WebClient();
                wc.DownloadFile("https://www.dropbox.com/s/sbo1lhps0yjn2hq/symbols.zip?dl=1", path + "\\sym.zip");
                ZipFile.ExtractToDirectory(path + "\\sym.zip", path);
                File.Delete(path + "\\sym.zip");
            }
            else if(File.Exists(path + "\\settings.conf"))
            {
                string[] settings = File.ReadAllLines(path + "\\settings.conf");
                foreach (string line in settings)
                {
                    if(line.IndexOf("baseIP:") == 0)
                    {
                        IpDialog.ip = line.Substring(line.IndexOf(":") + 1);
                    }
                }
            }
            InitializeComponent();
            cb_gameType.SelectedIndex = cb_gameWay.SelectedIndex = 0;
            tb_name.Foreground = tb_roomId.Foreground = Brushes.Gray;
            tb_name.Text = "choose nickname:";
            tb_roomId.Text = "room code:";
            InitGames();
        }

        private void InitGames()
        {
            cb_gameWay.Items.Add("player vs computer");
            cb_gameWay.Items.Add("player vs player");
            cb_gameWay.Items.Add("computer vs computer");

            ComboBoxItem a = new ComboBoxItem
            {
                Name = "default",
                Content = "choose game"
            };
            cb_gameType.Items.Add(a);

            a = new ComboBoxItem
            {
                Name = "chess",
                Content = "chess"
            };
            cb_gameType.Items.Add(a);
            GameInitDictionary.Add("chess", StartChess);

            a = new ComboBoxItem
            {
                Name = "checkers",
                Content = "checkers"
            };
            cb_gameType.Items.Add(a);
            GameInitDictionary.Add("checkers", StartCheckers);

            a = new ComboBoxItem
            {
                Name = "backgammon",
                Content = "backgammon"
            };
            cb_gameType.Items.Add(a);
            GameInitDictionary.Add("backgammon", StartBackgammon);

            a = new ComboBoxItem
            {
                Name = "submarines",
                Content = "submarines"
            };
            cb_gameType.Items.Add(a);
            GameInitDictionary.Add("submarines", StartSubmarines);
        }

        private void B_start_Click(object sender, RoutedEventArgs e)
        {
            if (tb_name.Text == "choose nickname:" || tb_name.Text == "")
            {
                MessageBox.Show("choose nickname");
            }
            else if(cb_gameType.SelectedIndex == 0)
            {
                MessageBox.Show("choose game");
            }
            else if(cb_gameWay.SelectedIndex == 0)
            {
                MessageBox.Show("choose players");
            }
            else
            {
                if (tb_roomId.Text == "room code:") tb_roomId.Text = "";
                gameId = tb_roomId.Text;
                userName = tb_name.Text;
                isPrivateGame = (chb_private.IsChecked == true);
                GameInitDictionary[gameName].DynamicInvoke();                
            }
        }

        private void B_instructions_Click(object sender, RoutedEventArgs e)
        {
            AdditionalPage p = new AdditionalPage();
            if (gameName == "chess")
            {
                p.text = StaticVars.ChessInstructions;
            }
            else if (gameName == "checkers")
            {
                p.text = StaticVars.CheckersInstructions;
            }
            else if (gameName == "backgammon")
            {
                p.text = StaticVars.BackgammonInstructions;
            }
            if (cb_gameType.SelectedIndex == 0)
                MessageBox.Show("choose game to show instructions");
            else p.Show();
        }

        #region Visuality
        private void HideStartScreen()
        {
            tb_name.Visibility = cb_gameType.Visibility = cb_gameWay.Visibility =
            b_instructions.Visibility = b_start.Visibility = tb_roomId.Visibility =
            Visibility.Collapsed;
        }

        #endregion

        #region game starts
        private void GeneralStart(GameBoardBase room, RoomType roomType)
        {
            if (cb_gameWay.SelectedIndex == 1)//player vs computer
            {

            }
            else if (cb_gameWay.SelectedIndex == 2)//player vs player
            {
                IpDialog ind = new IpDialog();
                ind.Show();
                hasSessionEnded = false;
                new Client(IpDialog.ip, GeneralThings.port, userName, roomType, tb_roomId.Text, chb_private.IsChecked == true, room);
                //new Thread(StartClient).Start();
            }
            else if (cb_gameWay.SelectedIndex == 3)//computer vs computer
            {

            }
        }

        private void StartCheckers()
        {
            CheckerdBoard checkersGame = new CheckerdBoard(RoomType.Chekers);
            //Checkers checkersGame = new Checkers();
            checkersGame.Show();
            GeneralStart(checkersGame, RoomType.Chekers);
        }

        private void StartSubmarines()
        {
            Submarines subGame = new Submarines();
            subGame.Show();
            GeneralStart(subGame, RoomType.Submarines);
        }

        private void StartChess()
        {
            MessageBox.Show("I love chess but 'wip'");
            //generalStart();
        }

        private void StartBackgammon()
        {
            MessageBox.Show("I love backgammon but 'wip'");
            //generalStart();
        }

        #endregion

        void StartClient()
        {
            ipAdress = new IPEndPoint(IPAddress.Parse("127.0.0.1"), GeneralThings.port);
            while (IpDialog.ip == "");
            string ipName = IpDialog.ip;
            try
            {
                IPAddress[] ipAddresses = Dns.GetHostAddresses(ipName);
                ipAdress = new IPEndPoint(ipAddresses[0], GeneralThings.port);
            }
            catch { }

            sender = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            infpr.Tick += Infpr_Tick;
            infpr.Interval = new TimeSpan(0, 0, 0, 0, 10);

            while (!hasConnected)
            {
                try
                {
                    sender.Connect(ipAdress);
                    hasConnected = true;
                    break;
                }
                catch
                {
                    Thread.Sleep(3000);
                }
            }

            if (hasConnected)
            {
                infpr.Start();
                new Thread(ClientReciever).Start();
            }
        }

        private void ClientReciever()
        {
            try
            {
                while (!hasSessionEnded)
                {
                    string data = null;
                    byte[] bytes = new byte[1024];
                    int bytesRec = sender.Receive(bytes);
                    data = Encoding.UTF8.GetString(bytes, 0, bytesRec);
                    if (data == "<name>`")
                    {
                        byte[] msg = Encoding.ASCII.GetBytes(userName);
                        sender.Send(msg);
                    }
                    else dataQ.Enqueue(data);
                }
            }
            catch { }
        }

        void Infpr_Tick(object sender, EventArgs e)
        {
            ClientInfoProceessing();
        }

        void ClientInfoProceessing()
        {
            if (dataQ.Count > 0)
            {
                string data = missedData + dataQ.Dequeue();
                missedData = "";
                if (data != "") ActualProccess(data);
            }
        }

        void ActualProccess(string data)
        {
            string command = data.Remove(data.IndexOf(">"));
            command = command.Remove(0, 1);
            string dataToContinueProcess = "";
            string action = (data.IndexOf(">") < data.Length - 1) ? data.Substring(data.IndexOf(">") + 1) : "";
            int count = 0;
            foreach (char ch in action)
            {
                if (ch == '<') count++;
            }
            if (count > 0)
            {
                dataToContinueProcess = action.Substring(action.IndexOf("<"));
                action = action.Remove(action.IndexOf("<"));
            }
            if (action[action.Length - 1] != '`')
            {
                missedData = "<" + command + ">" + action;
            }
            else
            {
                action = action.Replace("`", "");
                if (command == "checkersData")
                {
                    Checkers.gameData.Enqueue(action);
                }
                else if(command == "SubmarinesData")
                {
                    Submarines.gameData.Enqueue(action);
                }
                else if (command == "connected")
                {
                    if (gameId == "")
                    {
                        SendMessage("<login>" + ((isPrivateGame == true) ? "private" : "public") + "&" + gameName);
                    }
                    else
                    {
                        SendMessage("<login>" + ((isPrivateGame == true) ? "private" : "public")
                            + "&" + gameName + "&" + gameId);
                    }
                }
                else if (command == "endGame")
                {
                    string[] gameData = action.Split('&');
                    string endedGameID = gameData[0];
                    if (Games[endedGameID] is Checkers)
                    {
                        ((Checkers)Games[endedGameID]).EndGame(gameData[1]);
                    }
                    
                }
            }
            if (dataToContinueProcess != "") ActualProccess(dataToContinueProcess);
        }

        internal static void SendMessage(string output)
        {
            byte[] bytes;
            bytes = Encoding.UTF8.GetBytes(output + "`");
            try
            {
                sender.Send(bytes);
            }
            catch { }
        }

        private void Tb_name_GotFocus(object sender, RoutedEventArgs e)
        {
            if(tb_name.Foreground == Brushes.Gray)
            {
                tb_name.Foreground = Brushes.Black;
                tb_name.Text = "";
            }
        }

        private void Tb_name_LostFocus(object sender, RoutedEventArgs e)
        {
            if (tb_name.Foreground == Brushes.Black && tb_name.Text == "")
            {
                tb_name.Foreground = Brushes.Gray;
                tb_name.Text = "choose nickname:";
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if(!hasGameStarted)
            {
                if ((Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift))
                    && (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl)))
                {
                    if (e.Key == Key.S || e.Key == Key.H)
                    {
                        ServerActivator s = new ServerActivator();
                        s.Show();
                    }
                }
            }
        }

        private void Tb_roomId_GotFocus(object sender, RoutedEventArgs e)
        {
            if (tb_roomId.Foreground == Brushes.Gray)
            {
                tb_roomId.Foreground = Brushes.Black;
                tb_roomId.Text = "";
            }
        }

        private void Tb_roomId_LostFocus(object sender, RoutedEventArgs e)
        {
            if (tb_roomId.Foreground == Brushes.Black && tb_roomId.Text == "")
            {
                tb_roomId.Foreground = Brushes.Gray;
                tb_roomId.Text = "room code:";
            }
        }

        private void Cb_gameType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            gameName = ((ComboBoxItem)(e.AddedItems[0])).Name;
        }

        private void Settings_Click(object sender, RoutedEventArgs e)
        {
            SettingsPage sp = new SettingsPage();
            sp.Show();
        }
    }
}
