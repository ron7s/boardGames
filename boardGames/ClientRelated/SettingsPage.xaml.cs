﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net;
using System.Diagnostics;

namespace boardGames.ClientRelated
{
    /// <summary>
    /// Interaction logic for SettingsPage.xaml
    /// </summary>
    public partial class SettingsPage : Window
    {
        static readonly string path = AppDomain.CurrentDomain.BaseDirectory + "\\boardGamesData\\settings.conf";
        
        public SettingsPage()
        {
            InitializeComponent();
            if(File.Exists(path))
            {
                string[] settings = File.ReadAllLines(path);
                foreach (string line in settings)
	            {
                    if (line.IndexOf("baseIP:") == 0)
                    {
                        ipAdress.Text = line.Substring(line.IndexOf(":") + 1);
                    }
	            }
            }
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            if(File.Exists(path)) File.Delete(path);
            if(ipAdress.Text != "") File.AppendAllText(path, "baseIP:" + ipAdress.Text);
            MessageBox.Show("settings has been saved");
            IpDialog.ip = ipAdress.Text;
            this.Close();
        }

        private void Update_Click(object sender, RoutedEventArgs e)
        {
            WebClient wc = new WebClient();
            string updatePath = AppDomain.CurrentDomain.BaseDirectory + "\\ronsTmpUpdateDirWillBeDeletedShortly\\";
            Directory.CreateDirectory(updatePath);
            wc.DownloadFile("https://www.dropbox.com/s/mallejrnwmxkqqb/updateData.dat?dl=1", updatePath + "updateData.dat");
            wc.DownloadFile("https://www.dropbox.com/s/5d4ognl2h1q8i3c/softwareUpdater.exe?dl=1", updatePath + "softwareUpdater.exe");
            Process.Start(updatePath + "softwareUpdater.exe");
            Environment.Exit(0);
        }
    }
}
