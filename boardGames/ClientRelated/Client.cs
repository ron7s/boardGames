﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Windows;

namespace boardGames.ClientRelated
{
    public class Client
    {
        private static Socket listener;
        private static Dictionary<string, object> clientData;
        private Queue<Message> dataQ;
        private bool clientActive = false;
        private readonly gameBoards.GameBoardBase board;

        public static void SetData(string identifier, object value)
        {
            if (clientData.ContainsKey(identifier))
            {
                clientData[identifier] = value;
            }
            else
            {
                clientData.Add(identifier, value);
            }
        }

        public static object GetData(string identifier)
        {
            if(clientData.ContainsKey(identifier))
            {
                return clientData[identifier];
            }
            else
            {
                return null;
            }
        }

        public Client(string ip, int port, string userName, RoomType gameType, string roomId, bool isPrivate, gameBoards.GameBoardBase board)
        {
            bool hasConnected = InitClient(ip, port, userName, gameType, roomId, isPrivate);
            if (!hasConnected)
            {
                board.Close();
                MessageBox.Show("server seems to be inactive");
                return;
            }
            new Thread(ServerListener).Start();
            new Thread(MessageHandler).Start();
            this.board = board;
        }

        public static void SendMessage(Message.MessageType msgType, params byte[] data)
        {
            ComunicationHelper.SendMessage(listener, new Message(msgType, data));
        }

        public static void SendMessage(Message.MessageType msgType, object data)
        {
            switch (data.GetType().Name.ToLower())
            {
                case "string":
                    byte[] dataArr = Encoding.ASCII.GetBytes(data.ToString());
                    SendMessage(msgType, dataArr);
                    break;
                default:
                    SendMessage(msgType, (byte)data);
                    break;
            }

        }

        private bool InitClient(string ipStr, int port, string userName, RoomType gameType, string roomId, bool isPrivate)
        {
            IPEndPoint ip = new IPEndPoint(IPAddress.Parse(ipStr), port);
            clientData = new Dictionary<string, object>();
            dataQ = new Queue<Message>();
            listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            SetData("userName", userName);
            SetData("gameType", gameType);
            SetData("room", roomId);
            SetData("private", isPrivate);
            for (int i = 0; i < 5 && !listener.Connected; i++)
            {
                try
                {
                    listener.Connect(ip);
                }
                catch { }
            }
            if (!listener.Connected) return false;
            clientActive = true;
            SendStartupData(userName, gameType, roomId, isPrivate);
            return true;
        }

        private void SendStartupData(string userName, RoomType gameType, string roomId, bool isPrivate)
        {
            SendMessage(Message.MessageType.ClientData, userName);
            List<byte> data = new List<byte>();
            data.Add((byte)gameType);
            data.Add(Convert.ToByte(isPrivate));
            if (roomId != null && roomId != "")
                data.AddRange(Encoding.UTF8.GetBytes(roomId));
            SendMessage(Message.MessageType.StartGame, data.ToArray());//gameType, isPrivate, roomCode
        }

        private void ServerListener()
        {
            while (clientActive)
            {
                Thread.Sleep(20);
                List<byte> rcvd = ComunicationHelper.ReciveMessage(listener);
                if(rcvd == null)
                {
                    clientActive = false;
                    board.Close();
                    break;
                }
                Message msg = new Message(rcvd);
                lock (dataQ)
                {
                    dataQ.Enqueue(msg);
                }

            }
        }

        private void MessageHandler()
        {
            List<byte> previusData = null;
            while (clientActive)
            {
                Thread.Sleep(200);
                Message msg;
                lock (dataQ)
                {

                    if (previusData != null)
                    {
                        msg = new Message(previusData);
                    }
                    else if (dataQ.Count > 0)
                    {
                        msg = dataQ.Dequeue();
                    }
                    else//cancel, no message to proccess
                    {
                        continue;
                    }
                    msg = ObtainMessage(msg, ref dataQ, out previusData);
                }
                HandleMessage(msg);
            }
        }

        private Message ObtainMessage(Message msg, ref Queue<Message> msgs, out List<byte> previusData)
        {
            if (msg == null)
            {
                previusData = null;
                return null;
            }
            if (msg.Size >= 5 && msg.Size - 5 == msg.Data.Count)
            {
                previusData = null;
                return msg;
            }
            if ((msg.Size < 5 || msg.Size - 5 < msg.Data.Count) && msgs.Count > 0)
            {
                Message combined = new Message(msg.ToList(), msgs.Dequeue().ToList());
                return ObtainMessage(combined, ref msgs, out previusData);
            }
            if (msg.Size > 5 && msg.Size - 5 > msg.Data.Count)
            {
                previusData = new List<byte>();
                List<byte> tmp = msg.ToList();
                previusData.AddRange(tmp.GetRange((int)msg.Size, msg.Data.Count - (int)msg.Size));
                tmp.RemoveRange((int)msg.Size, msg.Data.Count - (int)msg.Size);
                return new Message(tmp);
            }
            if (msg.Size > 5 && msg.Size - 5 < msg.Data.Count)
            {
                previusData = msg.ToList();
                List<byte> msg1 = previusData.GetRange(0, (int)msg.Size);
                previusData.RemoveRange(0, (int)msg.Size);
                return new Message(msg1);
            }
            if(msg.Size < 5)
            {
                MessageBox.Show("empty messeage been rechived from the server");
                previusData = null;
                return null;
            }
            previusData = msg.ToList();
            return null;
        }

        private void HandleMessage(Message msg)
        {
            if (msg == null) return;
            switch (msg.MsgType)
            {
                //case Message.MessageType.ResetRequest:
                //    break;
                case Message.MessageType.Confirm:
                    HandleRequest(true);
                    break;
                case Message.MessageType.Cancel:
                    HandleRequest(false);
                    break;
                //case Message.MessageType.StartGame://gameType, isPrivate, roomCode
                //    break;
                case Message.MessageType.AnnouneWin://playerNum, userName
                    int playerNum = msg.Data[0];
                    msg.Data.RemoveRange(0, 1);
                    string userName = msg.GetAsText();
                    MessageBox.Show("the winner is player " + (playerNum + 1) + " named: " + userName);
                    board.Close();
                    break;
                case Message.MessageType.GameData:
                    board.EnqueueGamePieces(msg.Data.ToArray());
                    SetData("control", -1);
                    break;
                case Message.MessageType.AnnouneTurn:
                    SetData("control", msg.Data[0]);
                    break;
                case Message.MessageType.Request:
                    RequestFromUser((Message.Request)msg.Data[0]);
                    break;
                //case Message.MessageType.ClientData://name
                //    break;
                case Message.MessageType.MoveSuggestions:
                    board.EnqueueSuggestions(msg.Data.ToArray());
                    break;
                //case Message.MessageType.ChessMove:
                //    break;
                //case Message.MessageType.ChekersMove:
                //    break;
                //case Message.MessageType.BackgammonMove:
                //    break;
                //case Message.MessageType.SubmarinesHit:
                //    break;
                //case Message.MessageType.SubmarinesGameInfo:
                //    break;
                //case Message.MessageType.SubmarinesSetPositions:
                //    break;
                default:
                    break;
            }
        }

        private void HandleRequest(bool hasAccepted)
        {
            if(hasAccepted)
            {
                switch ((Message.Request)clientData["request"])
                {
                    case Message.Request.Restart:
                        break;
                    case Message.Request.Undo:
                        break;
                    case Message.Request.Redo:
                        break;
                    case Message.Request.None:
                    default:
                        break;
                }
                SetData("request", Message.Request.None);
            }
        }

        private void RequestFromUser(Message.Request request)
        {
            SetData("request", request);
            SendMessage(Message.MessageType.Request, request);
        }
    }
}
