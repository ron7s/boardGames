﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace boardGames.ClientRelated
{
    /// <summary>
    /// Interaction logic for WinAnnounce.xaml
    /// </summary>
    public partial class WinAnnounce : Window
    {
        public WinAnnounce(Brush color)
        {
            InitializeComponent();
            winColor.Fill = color;
        }
    }
}
