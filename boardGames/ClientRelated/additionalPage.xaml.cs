﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace boardGames.ClientRelated
{
    /// <summary>
    /// Interaction logic for additionalPage.xaml
    /// </summary>
    public partial class AdditionalPage : Window
    {
        internal string text = "";
        public AdditionalPage()
        {
            InitializeComponent();
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            instructions.Text = text;
            instructions.Width = this.ActualWidth / 1.5;
            instructions.Height = this.ActualHeight / 1.5;
        }
    }
}
