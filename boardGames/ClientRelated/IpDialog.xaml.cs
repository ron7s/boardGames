﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace boardGames.ClientRelated
{
    /// <summary>
    /// Interaction logic for ipDialog.xaml
    /// </summary>
    public partial class IpDialog : Window
    {
        internal static string ip = "";
        public IpDialog()
        {
            InitializeComponent();
        }

        private void b_connect_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(tb_ipAdress.Text))
            {
                ip = tb_ipAdress.Text;
                this.Close();
            }
            else MessageBox.Show("הכניסו כתובת IP תקינה" , "התחברות לשרת פרטי", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK, MessageBoxOptions.RtlReading);
        }

        public new void Show()
        {
            if (ip == "")
                base.Show();
        }
    }
}
