﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace boardGames
{
    public static class GeneralThings
    {
        public const int port = 47670;
    }

    public enum GamePieceType
    {
        CheckersPiece, CheckersKing,
        ChessPawn, ChessRunner, ChessRook, ChessKnight, ChessQueen, ChessKing,
        BacgammonPiece,
        Submarine
    }

    public enum RoomType
    {
        Chekers, Chess, Backgammon, Submarines
    }
}
