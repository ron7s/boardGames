﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace boardGames.ServerRelated
{
    class GamePart
    {
        readonly Dictionary<string, int> additionalData;

        public GamePart(int x, int y, RoomType type)
        {
            additionalData = new Dictionary<string, int>();
            additionalData.Add("type", (int)type);
            X = x;
            Y = y;
            if (type == RoomType.Chekers)
            {
                additionalData.Add("isKing", 0);
                additionalData.Add("isAlive", 1);
            }
        }

        public int X { get; set; }
        public int Y { get; set; }

        public int this[string identifier] 
        {
            get
            {
                return GetData(identifier);
            }
            set
            {
                SetData(identifier, value);
            }
        }

        public void SetData(string identifier, int value)
        {
            if (additionalData.ContainsKey(identifier))
                additionalData[identifier] = value;
            else
            {
                additionalData.Add(identifier, value);
            }
        }

        public int GetData(string identifier)
        {
            if (additionalData.ContainsKey(identifier))
                return additionalData[identifier];
            else
                return -1;
        }

    }
}
