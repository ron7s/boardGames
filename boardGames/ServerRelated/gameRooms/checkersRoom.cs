﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace boardGames.ServerRelated.gameRooms
{
    class CheckersRoom : GameRoom
    {
        List<int> kingChekersPlayers = new List<int>();
        Dictionary<int, string> locationsBlack = new Dictionary<int, string>();
        Dictionary<int, string> locationsWhite = new Dictionary<int, string>();

        public CheckersRoom(bool isPrivate, string id)
            : base(isPrivate, "checkers", id)
        {

        }

        public override void RestartGame()
        {
            kingChekersPlayers.Clear();
            int x = 0, y = 0;
            for (int i = 0; i < 12; i += 3)
            {
                locationsBlack.Add(i, (x + 1) + "," + y);
                locationsBlack.Add(i + 1, x + "," + (y + 1));
                locationsBlack.Add(i + 2, (x + 1) + "," + (y + 2));
                x += 2;
            }
            y = 5; x = 0;
            for (int i = 0; i < 12; i += 3)
            {
                locationsWhite.Add(i, x + "," + y);
                locationsWhite.Add(i + 1, (x + 1) + "," + (y + 1));
                locationsWhite.Add(i + 2, x + "," + (y + 2));
                x += 2;
            }
        }

        public Dictionary<int, string> BlackPlayers
        {
            get
            {
                return locationsBlack;
            }
        }
        public Dictionary<int, string> WhitePlayers
        {
            get
            {
                return locationsWhite;
            }
        }

        public string testPrintLocations()
        {
            string allLocations = "black:\n";
            foreach (string location in locationsBlack.Values)
            {
                allLocations += location + "\n";
            }
            allLocations += "\nwhite:\n";
            foreach (string location in locationsWhite.Values)
            {
                allLocations += location + "\n";
            }
            return allLocations;
        }

        public bool IschekersKing(int pl, string color)
        {
            if (color == "white")
            {
                if (kingChekersPlayers.Contains(pl + 12)) return true;
                else return false;
            }
            else
            {
                if (kingChekersPlayers.Contains(pl)) return true;
                else return false;
            }
        }
        public void addToChekersKingList(int pl, string color)
        {
            if (color == "white") kingChekersPlayers.Add(pl + 12);
            else kingChekersPlayers.Add(pl);
        }
    }
}
