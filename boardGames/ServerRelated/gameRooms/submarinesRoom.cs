﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace boardGames.ServerRelated.gameRooms
{
    class SubmarinesRoom : GameRoom
    {
        Dictionary<int, List<string>> locationsPlayer1 = new Dictionary<int, List<string>>();
        Dictionary<int, List<string>> locationsPlayer2 = new Dictionary<int, List<string>>();

        public SubmarinesRoom(bool isPrivate, string id)
            : base(isPrivate, "submarines", id)
        {

        }

        public void SetSubmarinesLocations(string data, string playerNum)
        {
            int count = 0;
            string[] subs = data.Split('+');
            foreach (string sub in subs)//location(A1,A2,A3...)
            {
                List<string> tmp = new List<string>();
                string[] locations = sub.Split(',');
                foreach (string pos in locations)
                {
                    tmp.Add(pos);
                }
                if (playerNum == "1")
                {
                    locationsPlayer1.Add(count, tmp);
                }
                else
                {
                    locationsPlayer2.Add(count, tmp);
                }
            }
        }

        public override void RestartGame()
        {
            
        }
    }
}
