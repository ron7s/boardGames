﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace boardGames.ServerRelated.gameRooms
{
    abstract class GameRoom
    {
        bool isPrivate = false, canGameStart = false, hasGameStarted = false;
        string gameType = "", id = "", additionalData = "";
        int player1 = -1, player2 = -1;
        List<int> connectedUsers = new List<int>();
        

        public GameRoom(bool isPrivate, string gameType, string id)
        {
            this.isPrivate = isPrivate;
            this.gameType = gameType;
            this.id = id;
            RestartGame();
        }

        public abstract void RestartGame();

        public string Rules
        {
            get
            {
                return additionalData;
            }
            set
            {
                additionalData = value;
            }
        }

        
        public bool IsPrivate
        {
            get
            {
                return isPrivate;
            }
        }
        public string GameType
        {
            get
            {
                return gameType;
            }
        }
        public bool CanStart
        {
            get
            {
                return canGameStart;
            }
        }
        public bool HasStarted
        {
            get
            {
                return hasGameStarted;
            }
            set
            {
                hasGameStarted = value;
            }
        }
        public string RoomId
        {
            get
            {
                return id;
            }
        }
        public int[] Users
        {
            get
            {
                return connectedUsers.ToArray();
            }
        }

        
        
        public int IsAPlayer(int user)
        {
            if (user == player1) return 1;
            else if (user == player2) return 2;
            else return -1;
        }

        public void addToRoom(int user)
        {
            if (player1 == -1) player1 = user;
            else if (player2 == -1)
            {
                player2 = user;
                canGameStart = true;
            }
            connectedUsers.Add(user);
        }
        public void removeFromRoom(int user)
        {
            if (player1 == user || player2 == user)
            {
                canGameStart = false;
                hasGameStarted = false;
                RestartGame();
                if (player1 == user) player1 = -1;
                else player2 = -1;
            }
            connectedUsers.Remove(user);
        }
    }
}
