﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Windows;

namespace boardGames.ServerRelated
{
    public class Server
    {
        private Socket listener;
        //private Dictionary<Socket, ClientsData> clients = new Dictionary<Socket, ClientsData>();
        private static ClientsData clients;
        private RoomsData rooms;
        private Random rnd;
        
        private bool serverActive = false;
        private int count;

        public Server(string ip, int port)
        {
            InitServer(ip, port);
            new Thread(ConnectionListener).Start();
        }

        private void InitServer(string ipStr, int port)
        {
            IPEndPoint ip = new IPEndPoint(IPAddress.Parse(ipStr), port);
            clients = new ClientsData();
            rooms = new RoomsData();
            rnd = new Random();
            listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            listener.Bind(ip);
            listener.Listen(10);
            count = 0;
            serverActive = true;
        }

        private void ConnectionListener()
        {
            while (serverActive)
            {
                Socket client = listener.Accept();
                clients.Add(client, count);
                count++;
                InitClients(client);
            }
        }

        private void InitClients(Socket client)
        {
            new Thread(() => ClientListener(client)).Start();
            new Thread(() => ClientMessageHandler(client)).Start();
        }

        private void ClientListener(Socket client)
        {
            while (serverActive)
            {
                Thread.Sleep(20);
                List<byte> rcvd = ComunicationHelper.ReciveMessage(client);
                if (rcvd == null)
                {
                    string roomId = clients[client, "room"].ToString();
                    rooms.GetPlayersInRoom(roomId).Remove((int)clients[client, "id"]);
                    clients.Remove(client);
                    break;
                }
                Message msg = new Message(rcvd);
                Queue<Message> msgs = ((Queue<Message>)clients[client, "msgs"]);
                lock (msgs)
                {
                    msgs.Enqueue(msg);
                }
                
            }
        }

        private void ClientMessageHandler(Socket client)
        {
            List<byte> previusData = null;
            while (serverActive)
            {
                Thread.Sleep(200);
                Queue<Message> msgs = ((Queue<Message>)clients[client, "msgs"]);
                Message msg;
                lock (msgs)
                {

                    if (previusData != null)
                    {
                        msg = new Message(previusData);
                    }
                    else if (msgs.Count > 0)
                    {
                        msg = msgs.Dequeue();
                    }
                    else//cancel, no message to proccess
                    {
                        continue;
                    }
#pragma warning disable CS0728 // Possibly incorrect assignment to local which is the argument to a using or lock statement
                    msg = ObtainMessage(msg, ref msgs, out previusData);
#pragma warning restore CS0728 // Possibly incorrect assignment to local which is the argument to a using or lock statement
                }
                HandleMessage(client, msg);
            }
        }

        private Message ObtainMessage(Message msg, ref Queue<Message> msgs, out List<byte> previusData)
        {
            if (msg == null)
            {
                previusData = null;
                return null;
            }
            if (msg.Size >= 5 && msg.Size - 5 == msg.Data.Count)
            {
                previusData = null;
                return msg;
            }
            if ((msg.Size < 5 || msg.Size - 5 < msg.Data.Count) && msgs.Count > 0)
            {
                Message combined = new Message(msg.ToList(), msgs.Dequeue().ToList());
                return ObtainMessage(combined, ref msgs, out previusData);
            }
            if(msg.Size > 5 && msg.Size - 5 > msg.Data.Count)
            {
                previusData = new List<byte>();
                List<byte> tmp = msg.ToList();
                previusData.AddRange(tmp.GetRange((int)msg.Size, msg.Data.Count - (int)msg.Size));
                tmp.RemoveRange((int)msg.Size, msg.Data.Count - (int)msg.Size);
                return new Message(tmp);
            }
            if(msg.Size > 5 && msg.Size - 5 < msg.Data.Count)
            {
                previusData = msg.ToList();
                List<byte> msg1 = previusData.GetRange(0, (int)msg.Size);
                previusData.RemoveRange(0, (int)msg.Size);
                return new Message(msg1);
            }
            if (msg.Size < 5)
            {
                MessageBox.Show("empty messeage been rechived from a client");
                previusData = null;
                return null;
            }
            previusData = msg.ToList();
            return null;
        }

        private void HandleMessage(Socket client, Message msg)
        {
            if (msg == null) return;
            switch (msg.MsgType)
            {
                //case Message.MessageType.ResetRequest:
                //    BroadcastMessage(client, msg.MsgType);
                //    clients[client, "request"] = Message.Request.Restart;
                //    break;
                case Message.MessageType.Confirm:
                    HandleRequest(client, true);
                    break;
                case Message.MessageType.Cancel:
                    HandleRequest(client, false);
                    break;
                case Message.MessageType.StartGame://gameType, isPrivate, roomCode
                    lock (rooms)
                    {
                        string newRoomsId = AddClientToRoom(client, msg.Data);
                        AttemptStartGame(newRoomsId);
                    }
                    break;
                //case Message.MessageType.GameData:
                //    break;
                //case Message.MessageType.AnnouneTurn:
                //    break;
                case Message.MessageType.Request:
                    BroadcastMessage(client, msg);
                    clients[client, "request"] = (Message.Request)msg.Data[0];
                    break;
                case Message.MessageType.ClientData://name
                    clients[client, "username"] = msg.GetAsText();
                    break;
                case Message.MessageType.MoveSuggestions:
                    byte[] locations = ((RoomsData.GetDataParamsAction)rooms[clients[client, "room"].ToString(), "sendMoveSuggestions"])(msg.Data.ToArray());
                    SendMessage(client, Message.MessageType.MoveSuggestions, locations);
                    break;
                //case Message.MessageType.ChessMove:
                //    break;
                case Message.MessageType.ChekersMove://playerNum, position, x, y
                    ((RoomsData.VoidParamsAction)rooms[clients[client, "room"].ToString(), "move"])(msg.Data.ToArray());
                    break;
                //case Message.MessageType.BackgammonMove:
                //    break;
                //case Message.MessageType.SubmarinesHit:
                //    break;
                //case Message.MessageType.SubmarinesGameInfo:
                //    break;
                //case Message.MessageType.SubmarinesSetPositions:
                //    break;
                default:
                    break;
            }
        }

        public static void SendMessage(Socket client, Message.MessageType msgType, params byte[] data)
        {
            ComunicationHelper.SendMessage(client, new Message(msgType, data));
        }

        public static void BroadcastMessage(Socket sender, Message.MessageType msgType, params byte[] data)
        {
            string room = clients[sender, "room"].ToString();
            Socket[] users = clients.GetAllUsers();
            foreach (Socket user in users)
            {
                if(room == clients[user, "room"].ToString())
                {
                    SendMessage(user, msgType, data);
                }
            }
        }

        public static void BroadcastMessage(string room, Message.MessageType msgType, params byte[] data)
        {
            Socket[] users = clients.GetAllUsers();
            foreach (Socket user in users)
            {
                if (room == clients[user, "room"].ToString())
                {
                    SendMessage(user, msgType, data);
                }
            }
        }

        private static void BroadcastMessage(Socket sender, Message msg)
        {
            BroadcastMessage(sender, msg.MsgType, msg.Data.ToArray());
        }

        public static void AnounceClientTurn(int id, int playerNum)
        {
            Socket[] users = clients.GetAllUsers();
            foreach (Socket user in users)
            {
                if((int)clients[user, "id"] == id)
                {
                    SendMessage(user, Message.MessageType.AnnouneTurn, (byte)playerNum);
                    break;
                }
            }
        }

        public static void AnounceWinner(string roomId, int playerId, int playerNum)
        {
            Socket[] users = clients.GetAllUsers();
            Socket winner = users[0];
            foreach (Socket user in users)
            {
                if ((int)clients[user, "id"] == playerId)
                {
                    winner = user;
                    break;
                }
            }
            List<byte> data = new List<byte> { (byte)playerNum };
            data.AddRange(Encoding.UTF8.GetBytes(clients[winner, "username"].ToString()));
            BroadcastMessage(winner, Message.MessageType.AnnouneWin, data.ToArray());
        }

        private void HandleRequest(Socket sender, bool hasAccepted)
        {
            string room = clients[sender, "room"].ToString();
            Socket[] users = clients.GetAllUsers();
            foreach (Socket user in users)
            {
                if (room == clients[user, "room"].ToString())
                {
                    if(hasAccepted && (Message.Request)clients[user, "request"] == Message.Request.Restart)
                    {
                        AttemptStartGame(room);
                    }
                    clients[user, "request"] = Message.Request.None;
                }
            }
        }

        private string AddClientToRoom(Socket client, List<byte> data)//roomType, isPrivate, roomCode
        {
            RoomType roomType = (RoomType)data[0];
            bool isPrivate = (data[1] == 1);
            data.RemoveRange(0, 2);
            string roomId = null;
            string[] roomsList = rooms.GetRoomIds();
            if (data.Count > 0)
            {
                roomId = Encoding.UTF8.GetString(data.ToArray());
            }
            else if (isPrivate)
            {
                roomId = GenerateRoomId();
            }
            else
            {
                if (roomsList != null)
                {
                    foreach (string id in roomsList)
                    {
                        if (!rooms.IsPrivateRoom(id) && rooms.GetRoomType(id) == roomType && !rooms.IsRoomFull(id))
                        {
                            roomId = id;
                            break;
                        }
                    }
                }
                if(roomId == null)
                {
                    roomId = GenerateRoomId();
                }
            }

            if(roomsList == null || !roomsList.Contains(roomId))
            {
                rooms.Add(roomId, isPrivate, roomType);
            }
            int clientId = (int)clients[client, "id"];
            rooms.GetPlayersInRoom(roomId).Add(clientId);
            clients[client, "room"] = roomId;
            return roomId;
        }

        private string GenerateRoomId()
        {
            string id;
            string[] roomIds = rooms.GetRoomIds();
            do
            {
                id = IdGenerator(10);
            }
            while (roomIds != null && roomIds.Contains(id));
            return id;
        }

        private string IdGenerator(int size)
        {
            string id = "";
            for (int i = 0; i < size; i++)
            {
                int type = rnd.Next(1, 3);
                if(type == 1)
                {
                    id += rnd.Next(0, 9).ToString();
                }
                else if(type == 2)
                {
                    id += Convert.ToChar(rnd.Next('A', 'Z'));
                }
                else
                {
                    id += Convert.ToChar(rnd.Next('a', 'z'));
                }
            }
            return id;
        }

        private void AttemptStartGame(string roomId)
        {
            if(rooms.IsRoomFull(roomId))
            {
                rooms[roomId, "playerTurn"] = 0;
                ((RoomsData.RoomAction)rooms[roomId, "startGame"])();
            }
        }

    }
}
