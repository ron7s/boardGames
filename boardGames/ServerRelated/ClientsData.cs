﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;

namespace boardGames.ServerRelated
{
    class ClientsData
    {
        readonly Dictionary<Socket, Dictionary<string, object>> usersData;

        public ClientsData()
        {
            usersData = new Dictionary<Socket, Dictionary<string, object>>();
        }

        public void Add(Socket client, int id)
        {
            if(!usersData.ContainsKey(client))
            {
                usersData.Add(client, new Dictionary<string, object>());
                usersData[client].Add("username", "user" + id);
                usersData[client].Add("id", id);
                usersData[client].Add("msgs", new Queue<Message>());
            }
        }

        public void Remove(Socket client)
        {
            if (usersData.ContainsKey(client))
            {
                usersData.Remove(client);
            }
        }

        public Socket[] GetAllUsers()
        {
            return usersData.Keys.ToArray();
        }

        public object this[Socket client, string identifier]
        {
            get
            {
                return GetData(client, identifier);
            }
            set
            {
                SetData(client, identifier, value);
            }
        }

        public void SetData(Socket client, string identifier, object value)
        {
            if (usersData.ContainsKey(client) && usersData[client].ContainsKey(identifier))
                usersData[client][identifier] = value;
            else if(usersData.ContainsKey(client))
                usersData[client].Add(identifier, value);
            else
            {
                usersData.Add(client, new Dictionary<string, object>());
                usersData[client].Add(identifier, value);
            }
        }

        public object GetData(Socket client, string identifier)
        {
            if (usersData.ContainsKey(client) && usersData[client].ContainsKey(identifier))
                return usersData[client][identifier];
            else
                return null;
        }
    }
}
