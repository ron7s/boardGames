﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Net;
using System.Net.Sockets;
using System.Windows.Threading;
using NATUPNPLib;
using boardGames.ServerRelated.gameRooms;

namespace boardGames.ServerRelated
{
    /// <summary>
    /// Interaction logic for serverActivator.xaml
    /// </summary>
    public partial class ServerActivator : Window
    {
        private object o = "";
        static Random rnd = new Random();

        private Socket listener;
        private Dictionary<int, Socket> handlers = new Dictionary<int, Socket>();
        private Dictionary<int, Queue<string>> recievedData = new Dictionary<int, Queue<string>>();
        private Dictionary<int, string> missedData = new Dictionary<int, string>();
        private Dictionary<string, string> gameIDs = new Dictionary<string, string>();
        private Dictionary<string, GameRoom> rooms = new Dictionary<string, GameRoom>();
        private List<string> names = new List<string>();
        private List<string> acceptedNames = new List<string>();
        private int count = 0;
        private string ipAsString = "192.168.0.101:" + GeneralThings.port;
        private bool isActive = false;

        private DispatcherTimer timer = new DispatcherTimer();

        public ServerActivator()
        {
            InitializeComponent();
            timer.Interval = new TimeSpan(0, 0, 0, 0, 10);
            timer.Tick += timer_Tick;
            tb_ipAdress.Text = ComunicationHelper.GetCurrentIp(false) + ":" + GeneralThings.port;

        }

        string generateID()
        {
            string newID = "";
            for (int i = 0; i < 10; i++)
            {
                int type = rnd.Next(1, 4);
                int num = 0;
                switch (type)
                {
                    case 1: num = rnd.Next(48, 58);
                        break;
                    case 2: num = rnd.Next(65, 91);
                        break;
                    default: num = rnd.Next(97, 123);
                        break;
                }

                newID += Convert.ToChar(num);
            }
            foreach (string id in gameIDs.Keys)
            {
                if (id == newID)
                {
                    newID = generateID();
                    break;
                }
            }

            return newID;
        }

        void actualProccess(string data, int user)
        {
            string action = "";
            string command = "";
            string dataToContinueProcess = "";
            try
            {
                command = data.Remove(data.IndexOf(">"));
                command = command.Remove(0, 1);
                dataToContinueProcess = "";
                action = (data.IndexOf(">") < data.Length - 1) ? data.Substring(data.IndexOf(">") + 1) : "";
                int count = 0;
                foreach (char ch in action)
                {
                    if (ch == '<') count++;
                }
                if (count > 0)
                {
                    dataToContinueProcess = action.Substring(action.IndexOf("<"));
                    action = action.Remove(action.IndexOf("<"));
                }
            }
            catch
            {
                return;
            }
            if (action[action.Length - 1] != '`')
            {
                missedData[user] = "<" + command + ">" + action;
            }
            else
            {
                action = action.Replace("`", "");

                if (command == "login") //private\public&gameType&id
                {
                    #region General Rooms Login
                    string[] info = action.Split('&');
                    string id = "";
                    bool doAddNewRoom = true;
                    bool isPrivate = (info[0] == "private");
                    if (info.Length < 3 && isPrivate)
                    {
                        while(rooms.ContainsKey(id) || id == "")
                            id = generateID();
                    }
                    else if (info.Length == 3)
                    {
                        id = info[2];
                        if (rooms.ContainsKey(id))
                        {
                            doAddNewRoom = false;
                        }
                    }
                    else
                    {
                        bool isInRoom = false;
                        foreach (GameRoom room in rooms.Values)
                        {
                            if (!room.IsPrivate && room.GameType == info[1] && room.Users.Length < 2)
                            {
                                id = room.RoomId;
                                doAddNewRoom = false;
                                isInRoom = true;
                                break;
                            }
                        }
                        if (!isInRoom)
                        {
                            id = generateID();
                        }
                    }
                    if(doAddNewRoom)
                    {
                        if (info[1] == "chess")
                        {
                            //rooms.Add(id, new chessRoom(isPrivate, id));
                        }
                        else if (info[1] == "checkers")
                        {
                            rooms.Add(id, new CheckersRoom(isPrivate, id));
                        }
                        else if (info[1] == "backgammon")
                        {
                            //rooms.Add(id, new backgammonRoom(isPrivate, id));
                        }
                        else if (info[1] == "submarines")
                        {
                            rooms.Add(id, new SubmarinesRoom(isPrivate, id));
                        }
                        gameIDs.Add(id, info[1]);
                    }
                    rooms[id].addToRoom(user);
                    #endregion

                    #region Checkers Handler
                    if (rooms[id].CanStart && !rooms[id].HasStarted && rooms[id].GameType == "checkers")
                    {
                        CheckersRoom room = (CheckersRoom)rooms[id];
                        room.HasStarted = true;
                        string newData = "<checkersData>";

                        foreach (int player in room.BlackPlayers.Keys)
                        {
                            newData += "black:n:" + player + "=" + room.BlackPlayers[player] + "&";
                        }
                        foreach (int player in room.WhitePlayers.Keys)
                        {
                            newData += "white:n:" + player + "=" + room.WhitePlayers[player] + "&";
                        }

                        foreach (int curUser in room.Users)
                        {
                            int roomPlNum = room.IsAPlayer(curUser);
                            string additionalText = "romId:" + id
                                + "&SData:" + ((roomPlNum == 1) ? "white" : (roomPlNum == 2) ? "black" : "spectator"
                                + "&CTurn:white");
                            sendMessage(curUser, newData + additionalText);
                        }

                    }
                    else if (rooms[id].HasStarted && rooms[id].GameType == "checkers")
                    {
                        CheckersRoom room = (CheckersRoom)rooms[id];
                        string newData = "<checkersData>";

                        foreach (int player in room.BlackPlayers.Keys)
                        {
                            newData += "black:n:" + player + "=" + room.BlackPlayers[player] + "&";
                        }
                        foreach (int player in room.WhitePlayers.Keys)
                        {
                            newData += "white:n:" + player + "=" + room.WhitePlayers[player] + "&";
                        }
                        string additionalText = "SData:spectator";
                        sendMessage(user, newData + additionalText);
                    }
                    #endregion
                }
                #region Checkers Move
                else if(command == "moveCheckers")
                {
                    //ID&color&number&x&y
                    string[] info = action.Split('&');
                    string id = info[0];
                    bool isWhiteTurn = true;
                    int pl = int.Parse(info[2]);
                    int newX = int.Parse(info[3]);
                    int newY = int.Parse(info[4]);
                    CheckersRoom room = (CheckersRoom)rooms[id];
                    if (info[1] == "white")
                    {
                        string oldLocation = room.WhitePlayers[int.Parse(info[2])];
                        int oldX = int.Parse(oldLocation.Remove(oldLocation.IndexOf(",")));
                        int oldY = int.Parse(oldLocation.Substring(oldLocation.IndexOf(",") + 1));
                        for (int i = 0; i < room.BlackPlayers.Count; i++)
                        {
                            string Bpl = room.BlackPlayers[i];
                            if (Bpl == "eaten") continue;
                            int Bx = int.Parse(Bpl.Remove(Bpl.IndexOf(",")));
                            int By = int.Parse(Bpl.Substring(Bpl.IndexOf(",") + 1));
                            if (newX - oldX > 0 && newY - oldY > 0)//moved right & down
                            {
                                if (Bx == newX - 1 && By == newY - 1)
                                {
                                    room.BlackPlayers[i] = "eaten";
                                    break;
                                }
                            }
                            else if (newX - oldX > 0)//moved right & up
                            {
                                if (Bx == newX - 1 && By == newY + 1)
                                {
                                    room.BlackPlayers[i] = "eaten";
                                    break;
                                }
                            }
                            else if (newY - oldY > 0)//moved left & down
                            {
                                if (Bx == newX + 1 && By == newY - 1)
                                {
                                    room.BlackPlayers[i] = "eaten";
                                    break;
                                }
                            }
                            else //moved left & up
                            {
                                if (Bx == newX + 1 && By == newY + 1)
                                {
                                    room.BlackPlayers[i] = "eaten";
                                    break;
                                }
                            }

                        }
                        room.WhitePlayers[pl] = info[3] + "," + info[4];
                        if (info[4] == "0") room.addToChekersKingList(int.Parse(info[2]), "white");
                        isWhiteTurn = false;
                        bool hasLost = true;
                        foreach (int pl2 in room.BlackPlayers.Keys)
                        {
                            if(room.BlackPlayers[pl2] != "eaten")
                            {
                                hasLost = false;
                                break;
                            }
                        }
                        if(hasLost)
                        {
                            foreach (int curUser in rooms[id].Users)
                            {
                                sendMessage(curUser, "<endGame>" + id + "&white");
                            }
                        }
                        
                    }
                    else
                    {
                        string oldLocation = room.BlackPlayers[int.Parse(info[2])];
                        int oldX = int.Parse(oldLocation.Remove(oldLocation.IndexOf(",")));
                        int oldY = int.Parse(oldLocation.Substring(oldLocation.IndexOf(",") + 1));
                        for (int i = 0; i < room.WhitePlayers.Count; i++)
                        {
                            string Bpl = room.WhitePlayers[i];
                            if (Bpl == "eaten") continue;
                            int Bx = int.Parse(Bpl.Remove(Bpl.IndexOf(",")));
                            int By = int.Parse(Bpl.Substring(Bpl.IndexOf(",") + 1));
                            if (newX - oldX > 0 && newY - oldY > 0)//moved right & down
                            {
                                if (Bx == newX - 1 && By == newY - 1)
                                {
                                    room.WhitePlayers[i] = "eaten";
                                    break;
                                }
                            }
                            else if (newX - oldX > 0)//moved right & up
                            {
                                if (Bx == newX - 1 && By == newY + 1)
                                {
                                    room.WhitePlayers[i] = "eaten";
                                    break;
                                }
                            }
                            else if (newY - oldY > 0)//moved left & down
                            {
                                if (Bx == newX + 1 && By == newY - 1)
                                {
                                    room.WhitePlayers[i] = "eaten";
                                    break;
                                }
                            }
                            else //moved left & up
                            {
                                if (Bx == newX + 1 && By == newY + 1)
                                {
                                    room.WhitePlayers[i] = "eaten";
                                    break;
                                }
                            }

                        }
                        room.BlackPlayers[int.Parse(info[2])] = info[3] + "," + info[4];
                        if (info[4] == "7") room.addToChekersKingList(int.Parse(info[2]), "black");
                        isWhiteTurn = true;
                        bool hasLost = true;
                        foreach (int pl2 in room.WhitePlayers.Keys)
                        {
                            if (room.WhitePlayers[pl2] != "eaten")
                            {
                                hasLost = false;
                                break;
                            }
                        }
                        if (hasLost)
                        {
                            foreach (int curUser in rooms[id].Users)
                            {
                                sendMessage(curUser, "<endGame>" + id + "&black");
                            }
                        }
                    }

                    string newData = "<checkersData>";

                    foreach (int player in room.BlackPlayers.Keys)
                    {
                        newData += "black:" + (room.IschekersKing(player, "black")? "k:" : "n:") + 
                            player + "=" + room.BlackPlayers[player] + "&";
                    }
                    foreach (int player in room.WhitePlayers.Keys)
                    {
                        newData += "white:" + (room.IschekersKing(player, "white") ? "k:" : "n:") +
                            player + "=" + room.WhitePlayers[player] + "&";
                    }

                    foreach (int curUser in room.Users)
                    {
                        int roomPlNum = room.IsAPlayer(curUser);
                        string additionalText = "SData:" + ((roomPlNum == 1) ? "white" :
                            (roomPlNum == 2) ? "black" : "spectator") + 
                            "&CTurn:" + ((isWhiteTurn) ? "white" : "black");
                        sendMessage(curUser, newData + additionalText);
                    }
                }
                #endregion

                #region submarinesAct
                else if(command == "submarinesAct")
                {
                    string[] actionsToComince = action.Split('&');
                    string id = "";
                    string playerNum = "";
                    SubmarinesRoom room = (SubmarinesRoom)rooms[id];
                    foreach (string act in actionsToComince)
                    {
                        string newCommand = act.Remove(act.IndexOf(":"));
                        string actualData = act.Substring(act.IndexOf(":") + 1);
                        if(newCommand == "setLocations")
                        {
                            room.SetSubmarinesLocations(actualData, playerNum);
                        }
                        else if(newCommand == "setGameRules")
                        {
                            room.Rules = actualData;
                            foreach (int usr in rooms[id].Users)
                            {
                                sendMessage(usr, "<rules>" + actualData);
                            }
                        }
                        else if(newCommand == "id")
                        {
                            id = actualData;
                        }
                        else if(newCommand == "playerNum")
                        {
                            playerNum = actualData;
                        }
                        
                    }
                }
                #endregion
            }
            if (dataToContinueProcess != "") actualProccess(dataToContinueProcess, user);
        }

        void timer_Tick(object sender, EventArgs e)
        {
            foreach (int user in recievedData.Keys)
            {
                if (recievedData[user].Count > 0)
                {
                    string data = recievedData[user].Dequeue();
                    actualProccess(data, user);
                }
            }
        }

        void stopServer()
        {
            if(listener != null && listener.Connected) listener.Close();
            isActive = false;
        }

        void startServer()
        {
            isActive = true;
            IPEndPoint ip = new IPEndPoint(IPAddress.Parse(ipAsString.Remove(ipAsString.IndexOf(":"))), int.Parse(ipAsString.Substring(ipAsString.IndexOf(":") + 1)));
            listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            listener.Bind(ip);
            listener.Listen(10);
            timer.Start();
            new Thread(serverActions).Start();
            this.Hide();
        }

        private void serverActions()
        {
            try
            {
                while (isActive)
                {
                    Socket handler = listener.Accept();
                    handlers.Add(count, handler);
                    names.Add("USER" + count);
                    count++;
                    new Thread(clientControl).Start();
                }
            }
            catch { }
        }

        private void clientControl()
        {
            int user = 0;
            lock (o) user = count - 1;
            Socket handler = handlers[user];
            byte[] bytes = new byte[1024];
            names[user] = setUserName(user);
            recievedData.Add(user, new Queue<string>());
            
            if (names[user] != null)
            {
                acceptedNames.Add(names[user]);
                string data = null;
                sendMessage(user, "<connected>");
                try
                {
                    while (isActive)
                    {
                        bytes = new byte[1024];
                        int bytesRec = handler.Receive(bytes);

                        data = Encoding.ASCII.GetString(bytes, 0, bytesRec);
                        recievedData[user].Enqueue(data);
                    }
                }
                catch
                {
                    acceptedNames.Remove(names[user]);
                    recievedData.Remove(user);
                    names[user] = "";
                }
            }

            handler.Shutdown(SocketShutdown.Both);
            handler.Close();
            handlers.Remove(user);
        }

        private string setUserName(int userNumber)
        {
            sendMessage(userNumber, "<name>");
            string input = reciveData(userNumber);
            bool isValidName = true;
            foreach (string name in names)
            {
                if (/*name == input || */input == "" || input == "name" || input == "addClient" || input == "removeClient")
                {
                    isValidName = false;
                    break;
                }
            }
            if (!isValidName) input += userNumber;
            return input;
        }

        internal void sendMessage(int userNumber, string output)
        {
            byte[] bytes;
            Socket user = handlers[userNumber];
            bytes = Encoding.UTF8.GetBytes(output + "`");
            try
            {
                user.Send(bytes);
            }
            catch { }
        }

        private string reciveData(int userNumber)
        {
            Socket user = handlers[userNumber];
            byte[] bytes = new byte[1024];
            int bytesRec = 0;
            try
            {
                bytesRec = user.Receive(bytes);
            }
            catch
            {
                return null;
            }
            return Encoding.ASCII.GetString(bytes, 0, bytesRec);
        }

        private void b_activate_Click(object sender, RoutedEventArgs e)
        {
            ipAsString = tb_ipAdress.Text;
            //startServer();
            new Server(ipAsString.Remove(ipAsString.IndexOf(":")), int.Parse(ipAsString.Substring(ipAsString.IndexOf(":") + 1)));
            this.Hide();

        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            tb_ipAdress.Text = ComunicationHelper.GetCurrentIp(cb.IsChecked == true) + ":" + GeneralThings.port;
        }
    }
}
