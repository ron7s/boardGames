﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace boardGames.ServerRelated
{
    class RoomsData
    {
        struct BoardPiece
        {
            public BoardPiece(int playerNum, int position)
            {
                PlayerNum = playerNum;
                Position = position;
            }

            public int PlayerNum { get; set; }
            public int Position { get; set; }
        }

        public delegate void RoomAction();
        public delegate void VoidParamsAction(params byte[] data);
        public delegate byte[] GetDataParamsAction(params byte[] data);
        static BoardPiece emptyBoardPiece = new BoardPiece(-1, -1);

        readonly Dictionary<string, Dictionary<string, object>> roomsData;

        public RoomsData()
        {
            roomsData = new Dictionary<string, Dictionary<string, object>>();
        }

        public void Add(string id, bool isPrivate, RoomType type)
        {
            if (!roomsData.ContainsKey(id))
            {
                roomsData.Add(id, new Dictionary<string, object>());
                roomsData[id].Add("type", type);
                roomsData[id].Add("playerTurn", -1);
                roomsData[id].Add("isPrivate", isPrivate);
                roomsData[id].Add("players", new List<int>());

                roomsData[id].Add("pl2Locations", new Dictionary<int, GamePart>());
                roomsData[id].Add("pl1Locations", new Dictionary<int, GamePart>());
                switch (type)
                {
                    case RoomType.Chekers:
                        roomsData[id].Add("startGame", new RoomAction(() => StartChekersGame(id)));
                        roomsData[id].Add("move", new VoidParamsAction((byte[] data) => ChekersMove(id, data)));
                        roomsData[id].Add("sendMoveSuggestions", new GetDataParamsAction((byte[] data) => GetChekersSuggestions(id, data)));
                        roomsData[id].Add("playersToStart", 2);
                        roomsData[id].Add("board", new BoardPiece[8, 8]);
                        break;
                    case RoomType.Chess:
                        roomsData[id].Add("startGame", new RoomAction(() => StartChessGame(id)));
                        roomsData[id].Add("boardPieces", new BoardPiece[8, 8]);
                        break;
                    case RoomType.Backgammon:
                        roomsData[id].Add("startGame", new RoomAction(() => StartBackgammonGame(id)));
                        break;
                    case RoomType.Submarines:
                        roomsData[id].Add("startGame", new RoomAction(() => StartSubmarinesGame(id)));
                        break;
                    default:
                        break;
                }
                InitBoardPieces(id);
            }
        }

        private void InitBoardPieces(string id)
        {
            BoardPiece[,] board = (BoardPiece[,])this[id, "board"];
            for (int x = 0; x < board.GetLength(0); x++)
            {
                for (int y = 0; y < board.GetLength(1); y++)
                {
                    board[x, y] = emptyBoardPiece;
                }
            }
        }

        public string[] GetRoomIds()
        {
            try
            {
                if(roomsData.Count > 0)
                {
                    return roomsData.Keys.ToArray();
                }
                return null;
            }
            catch
            {
                return null;
            }
        }

        public object this[string id, string identifier]
        {
            get
            {
                return GetData(id, identifier);
            }
            set
            {
                SetData(id, identifier, value);
            }
        }

        public void SetData(string id, string identifier, object value)
        {
            if (roomsData.ContainsKey(id) && roomsData[id].ContainsKey(identifier))
                roomsData[id][identifier] = value;
            else if (roomsData.ContainsKey(id))
                roomsData[id].Add(identifier, value);
            else
            {
                roomsData.Add(id, new Dictionary<string, object>());
                roomsData[id].Add(identifier, value);
            }
        }

        public object GetData(string id, string identifier)
        {
            if (roomsData.ContainsKey(id) && roomsData[id].ContainsKey(identifier))
                return roomsData[id][identifier];
            else
                return null;
        }

        public List<int> GetPlayersInRoom(string id)
        {
            return (List<int>)roomsData[id]["players"];
        }

        public bool IsPrivateRoom(string id)
        {
            return (bool)roomsData[id]["isPrivate"];
        }

        public RoomType GetRoomType(string id)
        {
            return (RoomType)roomsData[id]["type"];
        }

        public bool IsRoomFull(string id)
        {
            return (GetPlayersInRoom(id).Count >= (int)roomsData[id]["playersToStart"]);
        }

        #region Game Restarts
        private void StartChekersGame(string id)
        {
            Dictionary<int, GamePart> pl1 = (Dictionary<int, GamePart>)this[id, "pl1Locations"];
            Dictionary<int, GamePart> pl2 = (Dictionary<int, GamePart>)this[id, "pl2Locations"];
            pl1.Clear();
            pl2.Clear();
            int x = 0, y = 0;
            for (int i = 0; i < 12; i += 3)
            {
                pl2.Add(i, new GamePart(x + 1, y, RoomType.Chekers));
                pl2.Add(i + 1, new GamePart(x, y + 1, RoomType.Chekers));
                pl2.Add(i + 2, new GamePart(x + 1, y + 2, RoomType.Chekers));
                x += 2;
            }
            y = 5; x = 0;
            for (int i = 0; i < 12; i += 3)
            {
                pl1.Add(i, new GamePart(x , y, RoomType.Chekers));
                pl1.Add(i + 1, new GamePart(x + 1 , y + 1, RoomType.Chekers));
                pl1.Add(i + 2, new GamePart(x , y + 2, RoomType.Chekers));
                x += 2;
            }
            SendChekersPieces(id);
            InitBoard(id, pl1, pl2);
        }
        private void StartChessGame(string id)
        {

        }
        private void StartBackgammonGame(string id)
        {

        }
        private void StartSubmarinesGame(string id)
        {

        }
        #endregion

        #region Game Actions
        private void ChekersMove(string id, params byte[] data)//playerNum, position, newX, newY
        {
            Dictionary<int, GamePart> current = (Dictionary<int, GamePart>)this[id, "pl1Locations"];
            Dictionary<int, GamePart> other = (Dictionary<int, GamePart>)this[id, "pl2Locations"];
            bool ispl1 = (data[0] == 0);
            int otherNum = 1 - (int)roomsData[id]["playerTurn"];
            if (!ispl1)
            {
                current = (Dictionary<int, GamePart>)this[id, "pl2Locations"];
                other = (Dictionary<int, GamePart>)this[id, "pl1Locations"];
            }
            int curX = current[data[1]].X;
            int curY = current[data[1]].Y;
            int newX = data[2];
            int newY = data[3];

            bool isValidMove = false;
            byte[] locations = GetChekersSuggestions(id, data[0], data[1]);
            for (int i = 2; i < locations.Length; i+=2)
            {
                if(newX == locations[i] && newY == locations[i + 1])
                {
                    isValidMove = true;
                    break;
                }
            }

            if (isValidMove)
            {
                int eatX, eatY;
                if (curX > newX)
                {
                    if (curY > newY)
                    {
                        eatX = newX + 1;
                        eatY = newY + 1;
                    }
                    else
                    {
                        eatX = newX + 1;
                        eatY = newY - 1;
                    }
                }
                else
                {
                    if (curY > newY)
                    {
                        eatX = newX - 1;
                        eatY = newY + 1;
                    }
                    else
                    {
                        eatX = newX - 1;
                        eatY = newY - 1;
                    }
                }


                BoardPiece[,] board = (BoardPiece[,])this[id, "board"];
                //foreach (GamePart item in other.Values)
                //{
                //    if (item.X == eatX && item.Y == eatY)
                //    {
                //        item["isAlive"] = 0;
                //        board[eatX, eatY] = emptyBoardPiece;
                //        break;
                //    }
                //}
                int playerNum = GetPartPlayerOnPosition(id, eatX, eatY);
                if(playerNum == otherNum)
                {
                    other[board[eatX, eatY].Position]["isAlive"] = 0;
                    board[eatX, eatY] = emptyBoardPiece;
                }

                board[newX, newY] = board[current[data[1]].X, current[data[1]].Y];
                board[current[data[1]].X, current[data[1]].Y] = emptyBoardPiece;
                current[data[1]].X = newX;
                current[data[1]].Y = newY;
                if (ispl1 ? newY == 0 : newY == 7) current[data[1]]["isKing"] = 1;
                roomsData[id]["playerTurn"] = 1 - (int)roomsData[id]["playerTurn"];
            }
            SendChekersPieces(id);
            if(CheckCheckerdWin(other))
            {
                byte winner = (byte)(1 - (int)roomsData[id]["playerTurn"]);
                Server.AnounceWinner(id, winner, GetPlayersInRoom(id)[winner]);
            }
        }

        private void SendChekersPieces(string id)
        {//playerNum, position, isKing, X, Y
            Dictionary<int, GamePart> pl1 = (Dictionary<int, GamePart>)this[id, "pl1Locations"];
            Dictionary<int, GamePart> pl2 = (Dictionary<int, GamePart>)this[id, "pl2Locations"];
            List<byte> locations = new List<byte>();
            foreach (int pos in pl1.Keys)
            {
                if (pl1[pos]["isAlive"] == 0) continue;
                locations.Add(0);
                locations.Add((byte)pos);
                locations.Add((byte)pl1[pos]["isKing"]);
                locations.Add((byte)pl1[pos].X);
                locations.Add((byte)pl1[pos].Y);
            }
            foreach (int pos in pl2.Keys)
            {
                if (pl2[pos]["isAlive"] == 0) continue;
                locations.Add(1);
                locations.Add((byte)pos);
                locations.Add((byte)pl2[pos]["isKing"]);
                locations.Add((byte)pl2[pos].X);
                locations.Add((byte)pl2[pos].Y);
            }
            Server.BroadcastMessage(id, Message.MessageType.GameData, locations.ToArray());
            Server.AnounceClientTurn(GetPlayersInRoom(id)[(int)roomsData[id]["playerTurn"]], (int)roomsData[id]["playerTurn"]);
        }

        private byte[] GetChekersSuggestions(string id, params byte[] data)//playerNum, position
        {
            Dictionary<int, GamePart> pl1 = (Dictionary<int, GamePart>)this[id, "pl1Locations"];
            Dictionary<int, GamePart> pl2 = (Dictionary<int, GamePart>)this[id, "pl2Locations"];
            bool isPlayer1 = (data[0] == 0);
            int pos = data[1];
            GamePart part = (isPlayer1) ? pl1[pos] : pl2[pos];
            if (part["isAlive"] == 0)
            {
                return new byte[0];
            }
            bool isKing = (part["isKing"] == 1);
            List<byte> locations = new List<byte> { data[0], data[1] };

            #region getCorrectPoses
            int x = part.X, y = part.Y;
            bool dirUR = true, dirUL = true, dirDR = true, dirDL = true;
            if(!isKing)
            {
                dirDR = dirDL = !isPlayer1;
                dirUR = dirUL = isPlayer1;
            }
            int kingSteps = (isKing ? 7 : 1);
            for (int i = 1; i <= kingSteps; i++)
            {
                if(dirUR)
                {
                    dirUR = AddChekersPossibleMove(x, y, i, 1, -1, isPlayer1, ref locations, id);
                }
                if(dirUL)
                {
                    dirUL = AddChekersPossibleMove(x, y, i, -1, -1, isPlayer1, ref locations, id);
                }
                if(dirDR)
                {
                    dirDR = AddChekersPossibleMove(x, y, i, 1, 1, isPlayer1, ref locations, id);
                }
                if(dirDL)
                {
                    dirDL = AddChekersPossibleMove(x, y, i, -1, 1, isPlayer1, ref locations, id);
                }
            }
            #endregion

            return locations.ToArray();
        }

        private bool isPartOnPosition(Dictionary<int, GamePart> parts, int x, int y)
        {
            foreach (GamePart item in parts.Values)
            {
                if(item.X == x && item.Y == y && item["isAlive"] == 1)
                {
                    return true;
                }
            }
            return false;
        }

        private int GetPartPlayerOnPosition(string id, int x, int y)
        {
            BoardPiece[,] board = (BoardPiece[,])this[id, "board"];
            try
            {
                return board[x, y].PlayerNum;
            }
            catch(Exception)
            {
                return -1;
            }
        }

        private bool IsInChekeredBoard(int x, int y)
        {
            return x >= 0 && x < 8 && y >= 0 && y < 8;
        }

        private bool AddChekersPossibleMove(int x, int y, int i, int dx, int dy, bool isPlayer1, ref List<byte> locations, string id)
        {
            //Dictionary<int, GamePart> pl1 = (Dictionary<int, GamePart>)this[id, "pl1Locations"];
            //Dictionary<int, GamePart> pl2 = (Dictionary<int, GamePart>)this[id, "pl2Locations"];
            bool continueWithDir = true;
            int newX = x + i * dx, newY = y + i * dy;
            //if (IsInChekeredBoard(newX, newY) && !isPartOnPosition(pl1, newX, newY) && !isPartOnPosition(pl2, newX, newY))
            if (IsInChekeredBoard(newX, newY) && GetPartPlayerOnPosition(id, newX, newY) == -1)
            {
                locations.Add((byte)newX);
                locations.Add((byte)newY);
            }
            else
            {
                continueWithDir = false;
                //if (isPartOnPosition(isPlayer1 ? pl2 : pl1, newX, newY) && !isPartOnPosition(pl1, newX + dx, newY + dy) && !isPartOnPosition(pl2, newX + dx, newY + dy))
                if(GetPartPlayerOnPosition(id, newX, newY) == (isPlayer1? 1 : 0) && GetPartPlayerOnPosition(id, newX + dx, newY + dy) == -1)
                {
                    locations.Add((byte)(newX + dx));
                    locations.Add((byte)(newY + dy));
                }
            }
            return continueWithDir;
        }

        private bool CheckCheckerdWin(Dictionary<int, GamePart> parts)
        {
            foreach (GamePart item in parts.Values)
            {
                if (item["isAlive"] == 1)
                    return false;
            }
            return true;
        }

        private void InitBoard(string id, params Dictionary<int, GamePart>[] players)
        {
            BoardPiece[,] board = (BoardPiece[,])this[id, "board"];
            for (int i = 0; i < players.Length; i++)
            {
                foreach (int pos in players[i].Keys)
                {
                    board[players[i][pos].X, players[i][pos].Y] = new BoardPiece(i, pos);
                }
            }
        }
        #endregion

    }
}
