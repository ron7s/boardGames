﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace boardGames
{
    static class ComunicationHelper
    {
        public static IPAddress GetCurrentIp(bool isHamachi)
        {
            Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, 0);
            string baseAddress = (isHamachi) ? "25.0.0.0" : "192.0.0.0";
            s.Connect(baseAddress, 65530);
            IPEndPoint endPoint = s.LocalEndPoint as IPEndPoint;
            return endPoint.Address;
        }

        public static List<byte> ReciveMessage(Socket sender)
        {
            byte[] bytes = new byte[1512];
            List<byte> data = new List<byte>();
            try
            {
                int bytesAmount = sender.Receive(bytes);
                data.AddRange(bytes.ToList().GetRange(0, bytesAmount));
            }
            catch
            {
                return null;
            }
            return data;
        }

        public static void SendMessage(Socket receiver, Message msg)
        {
            try
            {
                receiver.Send(msg.ToArray());
            }
            catch { }
        }
    }
}
